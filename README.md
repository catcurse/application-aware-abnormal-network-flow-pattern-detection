# Documentation

The WoT Application Simulator publishes the flow instances through Kafka which is a message queue that follows publish/subscribe communication paradigm.

Matching Engine on the other end receives those flow instances through Kafka consumer.

### Step 1. Run kafka

##### Download Kafka(<https://kafka.apache.org/>)

##### (1) Run Zookeeper Server
	> bin/zookeeper-server-start.sh config/zookeeper.properties

##### (2) Run Kafka Server
	> bin/kafka-server-start.sh config/server.properties

##### (3) Create a topic
	> bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test

### Step 2. Execute Simulator

##### (1) Create white pattern list
	> sh genWhiteListShell.sh
	
OR

	> java -cp ./target/whiplash-0.0.1-SNAPSHOT-jar-with-dependencies.jar org.apl.netbig.platformx.genWhiteList 4 4 1000 500 0 0 50
	
genWhiteList's main method has 7 arguments. (... org.apl.netbig.platformx.genWhiteList __4 4 1000 500 0 0 50__
)

argument 1, 2 are flow length.

argument 3 is flow range.

argument 4 is number of white pattern.

argument 5 is skewed applicability. (0-skew O, 1-skew X)

argument 6 is random duration applicability. (0-random X, 1-random O)

argument 7 is input rate(interval).
	
### Step 3. Execute Algorithm

##### (1) Execute Whiplash
	> sh executeBaseShell.sh
	
OR

	> mvn package
	> java -cp ./target/whiplash-0.0.1-SNAPSHOT-jar-with-dependencies.jar org.apl.netbig.execute.ExecuteBase appInfo_F1000_A500_S0_D0_I10.txt 1000 5000 1 10 1 2>&1 | tee ./output/output_5_Base_F1000_A500_S0_D0_I10_5000.txt

##### (2) Execute TimedRETE
	> sh executeReteShell.sh
	
OR

	> mvn package
	> java -cp ./target/whiplash-0.0.1-SNAPSHOT-jar-with-dependencies.jar org.apl.netbig.execute.ExecuteReteOneThread appInfo_F1000_A500_S0_D0_I50.txt 1000 1000 1 10 1 2>&1 | tee ./output/output_4_Rete_F1000_A500_S0_D0_I50_1000.txt
	
ExecuteBase/Rete's main method have 6 arguments.(... org.apl.netbig.execute.ExecuteReteOneThread __appInfo_F1000_A500_S0_D0_I50.txt 1000 1000 1 10 1 2__)

argument 1 is name of white pattern list.

argument 2 is flow range.

argument 3 is interval of producing abnormal. (0-abnormal pattern X, 10000, 5000, 1000, 100)

argument 4 is interval of memory monitoring.

argument 5 is total experiment time.

argument 6 is simulated Application number limit option.