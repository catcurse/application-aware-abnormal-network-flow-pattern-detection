package org.apl.netbig.execute;

import org.apl.netbig.platformx.FlowSim;
import org.apl.netbig.rete.nodethread.*;


public class ExecuteRete {
	
	public static void main(String[] args) {
	//	String whiteListInputFileName = args[0];
	//	String flowInfoInput = args[1];
		String appInfoInput = args[0];
		long numberOfFlow = Long.parseLong(args[1]);
		int abnormalOption = Integer.parseInt(args[2]);
	//	int firstDurationOption = Integer.parseInt(args[3]);
	//	int executeJustOneOption = Integer.parseInt(args[4]);
		int showMemoryInterval = Integer.parseInt(args[3]); // 몇분에 한번씩 메모리 출력할지 (1분 = 1 입력)
		int showMemoryTotalTime = Integer.parseInt(args[4]);
		int appLimitOption = Integer.parseInt(args[5]);		
		
		Runnable matchingEngine = new MatchingEngine(appInfoInput);
    	Runnable flowSimulation = new FlowSim(appInfoInput, numberOfFlow, abnormalOption, /*firstDurationOption, executeJustOneOption,*/ appLimitOption);
		Runnable showMemory = new MonitorMemory(showMemoryInterval);
    	
    	Thread mathcingEngineThread = new Thread(matchingEngine);
    	Thread flowSimThread = new Thread(flowSimulation);
    	Thread showMemThread = new Thread(showMemory);
    	
    	mathcingEngineThread.setDaemon(true);
    	flowSimThread.setDaemon(true);
    	showMemThread.setDaemon(true);
    	
    	mathcingEngineThread.start();

    	try {
			Thread.sleep(5*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	flowSimThread.start();
    	showMemThread.start();
    	
    	try {
			Thread.sleep(showMemoryTotalTime*60*1000);
    	} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	try {
    		Thread.sleep(3*1000);
    	} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	MonitorMemory.printResult(); // 메모리테스트 결과 출력
	}
	
}
