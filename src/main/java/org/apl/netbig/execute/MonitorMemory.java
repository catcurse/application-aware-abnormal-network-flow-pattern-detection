package org.apl.netbig.execute;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

import org.apl.netbig.platformx.FlowConfig;
import org.apl.netbig.platformx.FlowSim;
import org.apl.netbig.rete.onethread.MatchingEngine;
import org.apl.netbig.whiplash.AbnormalityDetector;

public class MonitorMemory implements Runnable {
	 private static int showMemoryInterval = 0;
	 private static int totalNum = 0; // 메모리 평균 구하기 위한 변수(나누는 수)
	 private static long totalSum = 0; // 메모리 평균 구하기 위한 변수(더해진 수)
	 //private static long totalMax = 0; // 메모리 최대값 구하기 위한 변수
	
	 public static int numOfWrongAnswer = 0; // 정상을 비정상이라고 판단했을 경우
	 public static Object wrongLock = new Object();
	 
	 public static long totalOfTransport = 0; // platform에 몇번 물어보는 지
	 public static Object transportLock = new Object();
	 
	 public static ConcurrentHashMap<Long, Date> timeStampMap = new ConcurrentHashMap<Long, Date>(); // 소요시간 측정을 위한 <lid, timeStamp> 값을 저장한 자료구조.
	
	 public static long totalOfAbnormalRequiredTime = 0;
	 public static long totalOfNormalRequiredTime = 0;
	 public static long numOfAbnormalComplete = 0;
	 public static long numOfNormalComplete = 0;
	 
	 public static Object abnormalTimeCalculationLock = new Object();
	 public static Object normalTimeCalculationLock = new Object();
	 
	 static Runtime r = Runtime.getRuntime();
	 
	 public MonitorMemory(int interval){
		 showMemoryInterval = interval;
	 }
    
     public static void showMemory() {
    	   DecimalFormat format = new DecimalFormat("###,###,###.##");
    	 
	       //JVM이 현재 시스템에 요구 가능한 최대 메모리량, 이 값을 넘으면 OutOfMemory 오류가 발생 합니다.               
	       //long max = r.maxMemory();
	       //JVM이 현재 시스템에 얻어 쓴 메모리의 총량
	       //long total = r.totalMemory();
	       //JVM이 현재 시스템에 청구하여 사용중인 최대 메모리(total)중에서 사용 가능한 메모리
	       //long free = r.freeMemory();
	       
	       // 실제 사용중인 메모리 계산
	       long used = r.totalMemory() - r.freeMemory();
	       
	       long time = System.currentTimeMillis(); 
	       SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	       String currentTime = dayTime.format(new Date(time));
	   
	   /*
	       System.out.format("[Memory Test] Current Time : " + currentTime + "\n");
	       //System.out.format("\tTotal Memory : " + format.format(total) + " (%6.2f MB)\n", (double) total / (1024 * 1024));
	       //System.out.format("\tFree  Memory : " + format.format(free) + " (%6.2f MB)\n", (double) free  / (1024 * 1024));
	       System.out.format("\tUsed  Memory : " + format.format(used) + " (%6.2f MB)\n", (double) used   / (1024 * 1024));
	    */
	    //   System.err.println("Abnormal Map: " + FlowSim.abnormalMap); // abnormal 잘 찾았는 지 확인.
	     
	 /*      if(AbnormalityDetector.patternMap.isEmpty()) {
	    	   System.err.println("Pattern Map(first): null");
	       }else {
	    	   System.err.println("Pattern Map(first): " + AbnormalityDetector.patternMap.getFirst().toString());
	       }*/
	       
	       
	  //     System.err.println("Confirm Map: " + MatchingEngine.confirmMap);
	//       System.out.println("[Memory] " + currentTime + " Max : " + format.format(max) + ", Total : " + format.format(total) + ", Free : " + format.format(free));
	       
	       // 메모리 평균 구하기 위해 더해줌
	       totalSum += used;
	       totalNum += 1;
	       
	       // 메모리 최대값 구하기 위해 계산
	       //if(used > totalMax) totalMax = used;
     }

     public static void printResult() {
    	DecimalFormat format = new DecimalFormat("###,###,###.##");
    	 
		long totalAvg = totalSum / totalNum; // 메모리 사용 평균값 구함
		long time = System.currentTimeMillis(); 
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		String currentTime = dayTime.format(new Date(time));
		 
		System.out.format("\n[Memory Test Result] " + currentTime + "\n");
		System.out.format("\tTotal Average : " + format.format(totalAvg) + " (%6.2f MB)\n", (double) totalAvg / (1024 * 1024));
		//System.out.format("\tMax   Memory : " + format.format(totalMax) + " (%6.2f MB)\n", (double) totalMax  / (1024 * 1024));
		//   System.err.println("Abnormal Map: " + FlowSim.abnormalMap); // abnormal 잘 찾았는 지 확인.
		
		
		
		System.out.println("Average of transport: " + (double)((double)totalOfTransport / (double)FlowConfig.countOfLid));
		System.out.println("Total of transport: " + totalOfTransport);
		System.out.println("Last lid: " + FlowConfig.countOfLid);
		System.out.println("Number of instance: " + MatchingEngine.instanceID);
		
		if(numOfAbnormalComplete != 0) {
			System.out.println("Average of abnormal required time: " + totalOfAbnormalRequiredTime / numOfAbnormalComplete);
		}else {
			System.out.println("Number of abnormal flow: " + numOfAbnormalComplete);
		}
		
		if(numOfNormalComplete != 0) {
			System.out.println("Average of normal required time: " + totalOfNormalRequiredTime / numOfNormalComplete);
		}else {
			System.out.println("Number of normal flow: " + numOfNormalComplete);
		}
		System.out.println("number of wrong answer: " + numOfWrongAnswer);
		
		
		System.err.println("Pattern Map(first): " + AbnormalityDetector.patternQueue.toString());
		System.err.println("Confirm Map: " + MatchingEngine.confirmMap);

	//	System.out.println("[Memory Test Result] " + currentTime + " Total Average : " + format.format(totalAvg) + " , " + "Max : " + format.format(totalMax));
    	 
     }
     
	public void run() {
		Timer showMemoryTimer = new Timer();
		showMemoryTimer.schedule(new TaskOfShowMemory(), 0, showMemoryInterval*1000); // 1분마다 동작
	}
}
