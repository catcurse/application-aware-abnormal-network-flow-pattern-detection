package org.apl.netbig.execute;

import org.apl.netbig.platformx.FlowConfig;
import org.apl.netbig.platformx.FlowSim;
import org.apl.netbig.whiplash.AbnormalityDetector;

public class ExecuteBase {

	public static void main(String[] args) {
		String appInfoInput = args[0];
		long numberOfFlow = Long.parseLong(args[1]); // 플로우의 개수 옵션.
		int abnormalOption = Integer.parseInt(args[2]); // 비정상 상황도 섞는 옵션. (0, 1, 10, 100, 1000)
	//	int firstDurationOption = Integer.parseInt(args[3]); // first Duration을 기다리지 말고 바로 실행하게 해주는 옵션.
	//	int executeJustOneOption = Integer.parseInt(args[4]); // 앱 한번씩만 실행하게 하는 옵션.
		int showMemoryInterval = Integer.parseInt(args[3]); // 몇분에 한번씩 메모리 출력할지 (1초 = 1 입력)
		int showMemoryTotalTime = Integer.parseInt(args[4]); // 30분까지만 실행.
		int appLimitOption = Integer.parseInt(args[5]);

		Runnable abnormalityDetector = new AbnormalityDetector(appInfoInput);
    	Runnable flowSimulation = new FlowSim(appInfoInput, numberOfFlow, abnormalOption, /*firstDurationOption, executeJustOneOption,*/ appLimitOption);
		Runnable showMemory = new MonitorMemory(showMemoryInterval);
    	
    	Thread abnormalityDetectorThread = new Thread(abnormalityDetector);
    	Thread flowSimThread = new Thread(flowSimulation);
    	Thread showMemThread = new Thread(showMemory);

    	flowSimThread.setDaemon(true);
    	abnormalityDetectorThread.setDaemon(true);
    	showMemThread.setDaemon(true);
    	
    	abnormalityDetectorThread.start();
    	
    	try {
			Thread.sleep(5*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	flowSimThread.start();
    	showMemThread.start();

    	try {
			Thread.sleep(showMemoryTotalTime*60*1000);
    	} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	try {
    		Thread.sleep(3*1000);
    	} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	MonitorMemory.printResult(); // 메모리테스트 결과 출력
    	System.out.println("last lid: " + FlowConfig.countOfLid);
	}

}
