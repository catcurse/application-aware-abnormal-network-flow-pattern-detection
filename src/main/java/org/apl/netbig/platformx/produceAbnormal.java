package org.apl.netbig.platformx;

import java.util.Random;
import java.util.Timer;

public class produceAbnormal implements Runnable {
	
	
	private long interval;
	
	private synchronized long getCount() {
		synchronized(Thread.class) {
			FlowConfig.countOfLid+=1;
			return FlowConfig.countOfLid;
		}
	}
	
	public produceAbnormal(long interval) {
		this.interval = interval;
	}
	
	@Override
	public void run() {
		
		Random random = new Random();
		random.setSeed(123456789);
		
		while(true) {
						
			long randomFlwNum = (long)1 + (long)random.nextInt((int) (FlowSim.numberOfFlow));
			
			try {
				Thread.sleep(this.interval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			long lid = getCount();
			
			System.err.println("Abnormal flw:" + randomFlwNum + ", lid: " + lid);
			
			Timer produceAbnormalTimer = new Timer();
			produceAbnormalTimer.schedule(new TaskOfProduceAbnormal(randomFlwNum, lid), 0);
		}
		
	}

}
