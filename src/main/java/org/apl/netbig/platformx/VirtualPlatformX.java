package org.apl.netbig.platformx;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import org.apl.netbig.execute.MonitorMemory;
import org.json.simple.JSONObject;

public class VirtualPlatformX {	
	private static ConcurrentHashMap<Long, ArrayList<JSONObject>> appLogListMap = new ConcurrentHashMap<Long, ArrayList<JSONObject>>();
	private static Object appLogListMapLock = new Object();
	
	public static synchronized boolean matchingPlatformLog(long appID, ArrayList<Long> lidStack) { // 플로우가 정확히 매칭되는지 답을 알려준다.
		
		synchronized(MonitorMemory.transportLock) {
			MonitorMemory.totalOfTransport++;
		}

		long seqInLog = -1;
		int lidStackIndex = 0;
		
		ArrayList<JSONObject> appLogList = appLogListMap.get(appID); // 해당하는 appLogList 가져옴
		ArrayList<JSONObject> removeLogList = new ArrayList<JSONObject>(); // 매칭돼서 지워질 로그를 저장하는 리스트
				
		if(appLogList == null) return false;
		
	//	System.err.println(appID + ", " + appLogListMap + "\n" + appLogList);
		
		synchronized(appLogListMapLock) {
			Iterator<JSONObject> appLogListIterator = appLogList.iterator();
			
			// appLogList 순회
			for(; appLogListIterator.hasNext();) { 
				JSONObject logObject = appLogListIterator.next();
				long logLid = Long.parseLong(logObject.get("lid").toString());
										
				//for(; lidStackIndex < lidStack.size(); lidStackIndex++) { // lidStack 순회
				if(logLid == lidStack.get(lidStackIndex)) { // lidStack과 logList lid 비교
					if(seqInLog == -1) { // 일치하는 첫번째 플로우의 seq 저장
						seqInLog = Long.parseLong(logObject.get("seq").toString()); 			
					} else if(seqInLog != Long.parseLong(logObject.get("seq").toString())) { // 일치하는 두번째 플로우부터 적용
						// lid는 일치하는데 seq가 불일치 -> 비정상
					//	System.out.println("[PlatformX] RETURN : Abnormal(X) Pattern!");
						return false; // 비정상 반환					
					}
					removeLogList.add(logObject); // removeList에 해당 log object 추가
					lidStackIndex++; // 다음 lid로 넘어감
					
					if(lidStackIndex == lidStack.size()) break; // 전부 다 찾았으면 빠져나옴
				}
			}
					
			if(removeLogList.size() == lidStack.size()) { // 로그에 해당 패턴 존재 -> 정상 반환
			
				// removeLogList에 기록된 로그를 지움
				for(int i = 0; i < removeLogList.size(); i++) {
					appLogList.remove(removeLogList.get(i));
				}
				appLogListMap.put(appID, appLogList); // 매칭된 로그가 지워진 List를 다시 Map에 넣음
			//	System.out.println("[PlatformX] RETURN : Normal(O) Pattern!");
			
				return true;
			} else { // 로그에 해당 패턴 존재 X -> 비정상 반환
			//	System.out.println("[PlatformX] RETURN : Abnormal(X) Pattern!");
				return false; 
			}
		
		}
	}
	
	public static boolean matchingPlatformLogWithFlow(long appID, long lid) { // 플로우가 정확히 매칭되는지 답을 알려준다.
	//	String seqInLog = null;
	//	int lidStackIndex = 0;
		
		synchronized(appLogListMapLock) {
			
			ArrayList<JSONObject> appLogList = appLogListMap.get(appID); // 해당하는 appLogList 가져옴
		//	ArrayList<JSONObject> removeLogList = new ArrayList<JSONObject>(); // 매칭돼서 지워질 로그를 저장하는 리스트
			
			
			if(appLogList == null) return false;		
	
			// appLogList 순회		
			for(Iterator<JSONObject> appLogListIterator = appLogList.iterator(); appLogListIterator.hasNext();) { 
				JSONObject logObject = appLogListIterator.next();
				long logLid = Long.parseLong(logObject.get("lid").toString());
				
				//for(; lidStackIndex < lidStack.size(); lidStackIndex++) { // lidStack 순회
				if(logLid == lid) { // lid와 logList lid 비교
					return true; // 존재하면 true 반환
				}
			}
					
			return false; // 끝까지 찾았는데 존재하지 않으면 false 반환
		}
	}
	
	
	public static void addLogToAppLogListMap(long appNum, JSONObject appLog) {
		
		if(appNum == (long)-1) return;
		
		ArrayList<JSONObject> appLogList;
		
		synchronized(appLogListMapLock) {

			if(appLogListMap.containsKey((long)appNum)) {
				appLogList = appLogListMap.get((long)appNum); // 해당 app의 로그 리스트 가져옴
			} else {
				appLogList = new ArrayList<JSONObject>();
			}
			
			appLogList.add(appLog); // 리스트에 로그(한줄) 추가
			
			appLogListMap.put(appNum, appLogList);
	//	System.out.println("[PlatformX] Log write complete!");
		}
	}
}
