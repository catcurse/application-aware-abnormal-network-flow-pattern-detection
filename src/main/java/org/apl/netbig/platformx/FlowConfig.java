package org.apl.netbig.platformx;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

public class FlowConfig {
	// data type
	protected final static int svType = 0;
	protected final static int lid = 1;
	protected final static int s_date = 2;
	protected final static int e_date = 3;
	protected final static int norg = 4;
	protected final static int eorg = 5;
	protected final static int nip = 6;
	protected final static int eip = 7;
	protected final static int nport = 8;
	protected final static int eport = 9;
	protected final static int proto = 10;
	protected final static int app_grp = 11;
	protected final static int flw = 12;
	protected final static int in_byte = 13;
	protected final static int out_byte = 14;
	protected final static int in_pkt = 15;
	protected final static int out_pkt = 16;
	protected final static int url = 17;
	
//	protected final static SimpleDateFormat formatter = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss");
	
//	protected final static SimpleDateFormat formmaterMs = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss:SSS");
	
	public static long countOfLid = 0;
	
	protected static Properties createProducerConfig() {
		Properties props = new Properties();
	    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
	    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
	    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
	    return props;
	}
	
}
