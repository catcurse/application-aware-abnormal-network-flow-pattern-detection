package org.apl.netbig.platformx;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class genWhiteList {
	/*
	  	4 4 (0. 플로우 길이의 범위)
	  	1000 (1. 플로우 범위)
		1000 (2. 화이트리스트 개수)
		1 (3. skewed 여부)
		1 (4. 랜덤 듀레이션 옵션)
		1, 10, 100 (5. input rate 조절)
	 */
	public static ArrayList<ArrayList<Integer>> patternList = new ArrayList<ArrayList<Integer>>();
	public static ArrayList<ArrayList<Integer>> patternResult = new ArrayList<ArrayList<Integer>>();
	
	public static ConcurrentHashMap<ArrayList<Integer>, Integer> durationMap = new ConcurrentHashMap<ArrayList<Integer>, Integer>();
	
	public static void makePattern(ArrayList<Integer> pattern, int depth, int n, int k) {
		if(depth == k) { // 한번 depth가 k로 도달하면 사이클이 돌았음 -> 출력
			patternResult.add(pattern);
			return;
		}
		
		for(int i = depth; i < n; i++) {
			Collections.swap(pattern, i, depth);
			makePattern(pattern, depth+1, n, k);
			Collections.swap(pattern, i, depth);
		}
	}
			
	public static void main(String[] args) {
		int flowRangeX = Integer.parseInt(args[0]);
		int flowRangeY = Integer.parseInt(args[1]);
		int flowNum = Integer.parseInt(args[2]);
		int loopNum = Integer.parseInt(args[3]);
		int skewedOption = Integer.parseInt(args[4]);
		int durationRandomOption = Integer.parseInt(args[5]);
		int inputRateOption = Integer.parseInt(args[6]);
		
		Random random = new Random();
		BufferedWriter bufferedWriter = null;
			
		for(int i = flowRangeX; i <= flowRangeY; i++) { // 2~5(i) loop (flow pattern 개수의 경우의 수)
			for(int j = 0; j < loopNum; ) { // 원하는 횟수 만큼 반복(생성)
				ArrayList<Integer> pattern = new ArrayList<Integer>();
				
				if(skewedOption == 1) {
					for(int k = 0; k < i; ) { // 0~i loop (flow pattern 길이)
						int randomNum = random.nextInt(1000000) + 1; // 1~1000000 사이의 수를 랜덤으로 생성
						
						int remain = randomNum%100;
						
						if(remain > 50) { // 51~99
							randomNum = randomNum%3 + 1; // result : 1~3
						}else if(remain > 25) {
							randomNum = randomNum%7 + 4; // result : 4~10
						}else if(remain > 12) {
							randomNum = randomNum%10 + 11; // result : 11~20
						}else {
							randomNum = randomNum%flowNum + 1; // result : 1~flowNum
						}
						
						if(!pattern.contains(randomNum)) { // pattern에 이미 해당 수가 존재하지 않으면
							pattern.add(randomNum);
							k++;
						}
					} // pattern 인자 생성 끝
				}else if(skewedOption == 0){
					for(int k = 0; k < i; ) { // 0~i loop (flow pattern 길이)
						int randomNum = random.nextInt(flowNum) + 1; // 1~flownum 사이의 수를 랜덤으로 생성
						
						if(!pattern.contains(randomNum)) { // pattern에 이미 해당 수가 존재하지 않으면
							pattern.add(randomNum);
							k++;
						}
					}
				}
				
				// 이 패턴이 이미 만들었던 건지 확인
				if(!patternList.contains(pattern)) { // 이미 만들었던 패턴 아니면
					patternList.add(pattern);
					// pattern 만들기
					patternResult.add(pattern);
					//makePattern(pattern, 0, i, i); // 리스트, 재귀의 현재 깊이 나타냄,배열안에 들어있는 수, 몇개를 뽑아서 순열을 만들지(=배열 수) 
					
					j++;
				}

			}
		} // end of for 
		
		Collections.shuffle(patternResult); // patternResult 안에 있는 결과물을 섞음
		System.out.println("generated pattern list : " + patternResult);
		System.out.println("total amount of pattern list : " + patternResult.size());
		
		int interval = 10*1000;
		
		try {
			bufferedWriter = new BufferedWriter(new FileWriter("./appInfo_F" + flowNum + "_A" + loopNum + "_S" + skewedOption
					 + "_D" + durationRandomOption + "_I" + inputRateOption + ".txt", false));
				
			for(int k = 0; k < patternResult.size(); k++) { // patternResult 순회
				JSONObject jsonObject = new JSONObject();
				JSONArray jsonArrayFlow = new JSONArray();
				JSONArray jsonArrayDuration = new JSONArray();

				if(durationRandomOption==0) {
					for(int q = 0; q < patternResult.get(k).size(); q++) {
						jsonArrayFlow.add(patternResult.get(k).get(q));
					}
					for(int q = 0; q < jsonArrayFlow.size()-1; q++) {
						jsonArrayDuration.add(1);
					}
				}else {
					for(int q=0; q<patternResult.get(k).size(); q++) {
						jsonArrayFlow.add(patternResult.get(k).get(q));
						if(q != 0) {
							JSONArray jsonArrayFlowCopy = (JSONArray) jsonArrayFlow.clone();
							if(!durationMap.containsKey(jsonArrayFlowCopy)) {
								int random2 = random.nextInt(10) + 1;
								durationMap.put(jsonArrayFlowCopy, random2);
								jsonArrayDuration.add(random2);
							}else {
								jsonArrayDuration.add(durationMap.get(jsonArrayFlowCopy));
							}
						}
					}
				}
				
				if(inputRateOption == 1) {
					interval = 1*1000; // 1초.
				}else if(inputRateOption == 10) {
					interval = 10*1000; // 10초.
				}else if(inputRateOption == 100) {
					interval = 100*1000; // 100초.
				}
			
				int firstDuration = random.nextInt(10*1000)+1000; // 1초~10초
				
				jsonObject.put("app", k+1);
				jsonObject.put("flw", jsonArrayFlow);
				jsonObject.put("duration", jsonArrayDuration);
				jsonObject.put("interval", interval);
				jsonObject.put("firstDuration", firstDuration);
				
				bufferedWriter.write(jsonObject.toString());
				bufferedWriter.newLine(); // 한줄 개행
			}
			bufferedWriter.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
