package org.apl.netbig.platformx;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Random;
import java.util.Timer;

import org.json.simple.JSONArray;

public class AppAgent implements Runnable {
	private long appNum;
	private long interval;
	private long seqNum;
	private long duration;
	
//	private int executeJustOneOption = 0;
	
	public AppAgent(long appNum, long interval, long duration/*, int executeJustOneOption*/) {
		this.appNum = appNum;
		
		if(interval <= 0)
			this.interval = -1; // random interval
		else
			this.interval = interval;
		
		this.duration = duration;
	//	this.executeJustOneOption = executeJustOneOption;
	}
	
	private long getAppInfoIndex(long appNum) {
		if(FlowSim.appInfo.containsKey(appNum)) return appNum;
		else return -1;
	}
	
	/*private static BufferedWriter WriteTimeStampFile(String fileName) {
		
		BufferedWriter fw = null;
		try {
			fw = new BufferedWriter(new FileWriter(fileName, true));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fw;
	}*/
	
	private synchronized long getSeq() {
		synchronized(Thread.class) {
			this.seqNum+=1;
			return seqNum;
		}
	}
	
	@Override
	public void run() {
		boolean isParamsError = false;
		long appIndex = getAppInfoIndex(appNum);
		
		if(appIndex == -1) {
			System.err.println("App" + this.appNum + " is not found.");
			isParamsError = true;
		}
		
		JSONArray jsonFlow = (JSONArray) FlowSim.appInfo.get(appIndex).get("flw");
		JSONArray jsonDuration = (JSONArray) FlowSim.appInfo.get(appIndex).get("duration");
		
		long []flw = new long[jsonFlow.size()];
		long []duration = new long[jsonDuration.size()];
		
		for(int i=0; i<jsonFlow.size(); i++)
			flw[i] = Integer.parseInt(jsonFlow.get(i).toString());
		
		for(int i=0; i<jsonDuration.size(); i++)
			duration[i] = Integer.parseInt(jsonDuration.get(i).toString());
		
		
		//BufferedWriter fw = WriteTimeStampFile("app" + this.appNum + ".txt");
		
		if(isParamsError == false) {
		/*	if(executeJustOneOption == 1) {
				long seq = this.getSeq();				
				//Runnable app = new WFTask(this.appNum, flw, duration, fw, seq);
				
				try {
					Thread.sleep(this.duration);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
				Runnable app = new WFTask(this.appNum, flw, duration, seq);
				Thread t = new Thread(app);
				t.start();
				try {
					Thread.sleep(this.interval);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}else {*/
			while(true) {
				long seq = this.getSeq();				
				//Runnable app = new WFTask(this.appNum, flw, duration, fw, seq);
				
				try {
					Thread.sleep(this.duration);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
				Runnable app = new WFTask(this.appNum, flw, duration, seq);
				Thread t = new Thread(app);
				t.start();
				try {
					Thread.sleep(this.interval);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	//	}
		
		/*
		// added by Hana (App의 비주기 실행)
		if(isParamsError == false) {
			if(this.appNum == 3) { // 비주기적으로 실행되는 앱
				Random random = new Random();
				
				while(true) {
					int randomInterval = random.nextInt(60*60*1000); // 비주기 task의 time interval은 1시간 이내
					WFTask notPeriodTask = new WFTask(appNum, flw, duration);
					notPeriodTask.run();
			
					try {
						Thread.sleep(randomInterval*1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else { // 주기적으로 실행되는 앱
				Timer jobSchedular = new Timer();
				jobSchedular.schedule(new WFTask(appNum, flw, duration), firstTaskTime.getTime(), interval);
			}
		}
		*/
	}
}
