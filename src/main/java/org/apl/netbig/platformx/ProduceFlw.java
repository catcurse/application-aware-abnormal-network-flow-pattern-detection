package org.apl.netbig.platformx;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.json.simple.JSONObject;

public class ProduceFlw {
	
	private SimpleDateFormat formmaterMs = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss:SSS");
	
	public void produceFlw(long flwNum, long appNum, long count, long seq) {
	//public void produceFlw(int flwNum, int appNum, int count, int seq, BufferedWriter fw) {
		// make message to kafka
		String []producedString = new String[18];
		
		// variable value
		long flwIndex = flwNum;
		if(flwIndex < 0 || flwIndex > FlowSim.numberOfFlow) { // return if flw number missing
			System.err.println("ERROR : flw number missing! don't generate flow!: " + flwNum);
			return;
		}else {
			producedString[FlowConfig.flw] = String.valueOf(flwNum);
		//	producedString[cf.nport] = FlowSim.flowInfo.get(flwIndex).get("nport").toString();
		//	producedString[cf.eport] = FlowSim.flowInfo.get(flwIndex).get("eport").toString();
		}
		
		// timestamp
        String s_date = this.formmaterMs.format(new Date());
                
        // time만큼 sleep
        try {
			Thread.sleep(Integer.parseInt(FlowSim.flowInfo.get(flwIndex).get("time").toString()) * 1000);
		} catch (NumberFormatException | InterruptedException e1) {
			e1.printStackTrace();
		}

        // 1초 뒤 timestamp
        String e_date = this.formmaterMs.format(new Date());
        
		// lid
		String lid = String.valueOf(count);
		producedString[FlowConfig.lid] = lid;
		
		// time value
		producedString[FlowConfig.s_date] = s_date;
		producedString[FlowConfig.e_date] = e_date;
		
		/*
		// fixed value
		producedString[cf.svType] = "flw_12";
		producedString[cf.nip] = "127.0.0.1";
		producedString[cf.eip] = "127.0.0.1";
		producedString[cf.norg] = "100";
		producedString[cf.eorg] = "100";
		producedString[cf.app_grp] = "2";
		
		// temporary fixed value
		producedString[cf.in_byte] = "2114";
		producedString[cf.out_byte] = "44023";
		producedString[cf.in_pkt] = "22";
		producedString[cf.out_pkt] = "33";
		producedString[cf.proto] = "6";
		producedString[cf.url] = "";
		*/
		JSONObject mergedString = new JSONObject();
		
		mergedString.put("app", appNum);
		mergedString.put("flw", Integer.parseInt(producedString[FlowConfig.flw]));
		mergedString.put("lid", producedString[FlowConfig.lid]);
		mergedString.put("s_date", producedString[FlowConfig.s_date]);
		mergedString.put("e_date", producedString[FlowConfig.e_date]);
		mergedString.put("seq", seq);
		/*
		mergedString.put("svType", producedString[cf.svType]);
		mergedString.put("norg", Integer.parseInt(producedString[cf.norg]));
		mergedString.put("eorg", Integer.parseInt(producedString[cf.eorg]));
		mergedString.put("nip", producedString[cf.nip]);
		mergedString.put("eip", producedString[cf.eip]);
		mergedString.put("nport", Integer.parseInt(producedString[cf.nport]));
		mergedString.put("eport", Integer.parseInt(producedString[cf.eport]));
		mergedString.put("proto", Integer.parseInt(producedString[cf.proto]));
		mergedString.put("app_grp", Integer.parseInt(producedString[cf.app_grp]));
		mergedString.put("in_byte", Integer.parseInt(producedString[cf.in_byte]));
		mergedString.put("out_byte", Integer.parseInt(producedString[cf.out_byte]));
		mergedString.put("in_pkt", Integer.parseInt(producedString[cf.in_pkt]));
		mergedString.put("out_pkt", Integer.parseInt(producedString[cf.out_pkt]));
		mergedString.put("url", producedString[cf.url]);*/
		
		
		JSONObject json = new JSONObject();
        
        json.put("app", appNum);
        json.put("flw", flwNum);
        json.put("lid", lid);
        json.put("s_date", s_date);
        json.put("e_date", e_date);
        json.put("seq", seq);
        
        VirtualPlatformX.addLogToAppLogListMap(appNum, json); // 메모리에 존재하는 appLogListMap에 추가
        
		String fString = mergedString.toJSONString();
		
		// set up Kafka producer
	    KafkaProducer<String,String> kafkaProducer;
		Properties prop = FlowConfig.createProducerConfig();
		kafkaProducer = new KafkaProducer<String, String>(prop);
				
		if(appNum == -1) { // 비정상 플로우일 경우 Abnormal Map에 넣어줌.(잘 작동하는 지 확인을 위해서)
        	FlowSim.abnormalMap.put(count, (long)0);
        }
		
		// add data to Kafka
        ProducerRecord<String,String> producerRecord = new ProducerRecord<String,String>("test", fString);
        kafkaProducer.send(producerRecord);
        
   //     System.out.println(s_date + ":");
   //     System.out.println(fString + "\n");
        
        // close Kafka
        kafkaProducer.close();

        /*
        try {
			fw.write(json.toString());
			fw.write("\n");
	        fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
    	
	}
}
