package org.apl.netbig.platformx;

import java.util.TimerTask;

public class TaskOfProduceAbnormal extends TimerTask {

	private ProduceFlw pf = new ProduceFlw();
	
	private long randomFlwNum;
	private long lid;
	
	public TaskOfProduceAbnormal(long randomFlwNum, long lid) {
		this.randomFlwNum = randomFlwNum;
		this.lid = lid;
	}
	
	@Override
	public void run() {
		pf.produceFlw(this.randomFlwNum, -1, this.lid, -1);
	}
	
}
