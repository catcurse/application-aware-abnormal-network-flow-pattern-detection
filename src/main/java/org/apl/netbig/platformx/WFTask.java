package org.apl.netbig.platformx;

import java.io.BufferedWriter;

public class WFTask implements Runnable {

	private ProduceFlw pf = new ProduceFlw();
	
	private long flw[];
	private long duration[];
	private long appNum;
	private long seqNum;
//	private BufferedWriter fw;
	
	//public WFTask(int appNum, int flw[], int duration[], BufferedWriter fw, int seq) {
	public WFTask(long appNum, long flw[], long duration[], long seq) {
		this.flw = flw;
		this.duration = duration;
		this.appNum = appNum;
		//this.fw = fw;
		this.seqNum = seq;
	}

	private synchronized long getCount() {
		synchronized(Thread.class) {
			FlowConfig.countOfLid+=1;
			return FlowConfig.countOfLid;
		}
	}
	
	private int getFlowInfoIndex(int flwNum) {
		for(int i=0; i<FlowSim.flowInfo.size(); i++) {
			if(flwNum == Integer.parseInt(FlowSim.flowInfo.get(i).get("app").toString())) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public void run() {
		try {
			for(int i=0; i<flw.length; i++) {
				//pf.produceFlw(flw[i], appNum, getCount(), this.seqNum, fw);
				pf.produceFlw(flw[i], appNum, getCount(), this.seqNum);
				if(i == flw.length-1)
					continue;
				else {
				//	int time = Integer.parseInt(FlowSim.flowInfo.get(getFlowInfoIndex(flw[i])).get("time").toString());
					Thread.sleep(1000*(duration[i]));
				}
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
