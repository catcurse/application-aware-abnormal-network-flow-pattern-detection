package org.apl.netbig.platformx;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class FlowSim implements Runnable {	
	
	protected static ConcurrentHashMap<Long, JSONObject> flowInfo = new ConcurrentHashMap<Long, JSONObject>();
	public static ConcurrentHashMap<Long, JSONObject> appInfo = new ConcurrentHashMap<Long, JSONObject>();
	
	public static ConcurrentHashMap<Long, Long> abnormalMap = new ConcurrentHashMap<Long, Long>();	
	
    private String appInfoInput;
    protected static long numberOfFlow;
	private int abnormalOption = 0; // 비정상패턴도 실행하는 옵션. (0, 1, 10)
//	private int firstDurationOption = 0; // 바로 실행하는 옵션.
//	private int executeJustOneOption = 0; // 앱 한번만 실행하는 옵션.
	private int appLimitOption = 0; // app 갯수 limit 옵션.

	public FlowSim(String appInfoInput, long num, int abnormalOption, /*int firstDurationOption, int executeJustOneOption,*/ int appLimitOption) {
	//	this.flowInfoInput = flowInfoInput;
		this.appInfoInput = appInfoInput;
		makeFlowInfo(num);
		this.numberOfFlow = num;
		this.abnormalOption = abnormalOption;
	//	this.firstDurationOption = firstDurationOption;
	//	this.executeJustOneOption = executeJustOneOption;
		this.appLimitOption = appLimitOption;
	}
	
	private void makeFlowInfo(long numberOfFlow) {
		for(long i=1; i<numberOfFlow+1; i++) {
			JSONObject flowInfoInstance = new JSONObject();
			flowInfoInstance.put("flw", i);
			flowInfoInstance.put("time", 1);
			flowInfo.put(i, flowInfoInstance);
		}
	}
    
	private ConcurrentHashMap<Long, JSONObject> ReadInfoFile(String inputFileName) {
		
		ConcurrentHashMap<Long, JSONObject> tempInfo = new ConcurrentHashMap<Long, JSONObject>();
		JSONParser jsonParser = new JSONParser();

		try{
			FileReader fileReader = new FileReader(inputFileName);
			BufferedReader bufferedFileReader = new BufferedReader(fileReader);
			
			String tempString = new String();
			long appNumber = 0;
			// White list 파일을 끝까지 읽어서 json array인 white_pattern_list에 저장.
			while(true) {
				tempString = bufferedFileReader.readLine();
				if(tempString == null) break;
				appNumber++;
				
				JSONObject tempJsonObject = (JSONObject) jsonParser.parse(tempString);
				tempInfo.put(appNumber, tempJsonObject);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return tempInfo;
	}
	
	@Override
	public void run() {
		
		this.appInfo = ReadInfoFile(this.appInfoInput);
		int appSize;
		
		if(appLimitOption == 1) {
			Random random = new Random(123456789);
			for(int i=0; i<100; i++) {
				long appNum = (long)random.nextInt(this.appInfo.size()) + (long)1;
				
				long interval = Long.parseLong(this.appInfo.get(appNum).get("interval").toString());
				long duration;
			//	if(firstDurationOption != 1) { // 바로 실행하는 옵션.
				duration = Long.parseLong(this.appInfo.get(appNum).get("firstDuration").toString());
			//	}else {
			//		duration = 0;
			//	}
				
				Runnable app = new AppAgent(appNum, interval, duration/*, this.executeJustOneOption*/);
				Thread t = new Thread(app);
			//	t.setDaemon(true);
				t.start();
			}
		}else {
			appSize = this.appInfo.size();
			for(long i=0; i<appSize; i++) {
				long interval = Long.parseLong(this.appInfo.get(i+1).get("interval").toString());
				long duration;
			/*	if(firstDurationOption != 1) { // 바로 실행하는 옵션.
					duration = Long.parseLong(this.appInfo.get(i+1).get("firstDuration").toString());
				}else {
					duration = 0;
				}*/
				duration = Long.parseLong(this.appInfo.get(i+1).get("firstDuration").toString());
				
				Runnable app = new AppAgent(i+1, interval, duration);
				Thread t = new Thread(app);
			//	t.setDaemon(true);
				t.start();
			}
		}
		
		if(this.abnormalOption == 0) {
			
		}else {
			Runnable produceAbnormal = new produceAbnormal((long)abnormalOption);
			Thread prodAbnormalThread = new Thread(produceAbnormal);
			prodAbnormalThread.setDaemon(true);
			prodAbnormalThread.start();
		}
	}    
}