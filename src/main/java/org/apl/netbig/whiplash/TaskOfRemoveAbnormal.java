package org.apl.netbig.whiplash;

import java.util.TimerTask;

import org.json.simple.JSONObject;

public class TaskOfRemoveAbnormal extends TimerTask  {
	JSONObject flowObject = new JSONObject();
	
	public TaskOfRemoveAbnormal(JSONObject flowObject) { // 생성자
		this.flowObject = flowObject;
	}

	public void run() {
		AbnormalityDetector.removeAbnormalPattern(this.flowObject);
	}

}
