package org.apl.netbig.whiplash;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class FlowReceiver {

	/*
	private static final String TOPIC = "test";
	private static final int NUM_THREADS = 20;
	
	private static Properties createConsumerConfig() {
		Properties props = new Properties();
	    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
	    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
	    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
	    return props;
	}
	*/
	
	public static void main(String[] args) throws Exception {
				/*
		// set up Kafka producer
	    KafkaConsumer<String,String> kafkaConsumer;
	    
		Properties prop = createConsumerConfig();
		kafkaConsumer = new KafkaConsumer<>(prop);
		kafkaConsumer.subscribe(Arrays.asList("test"));
		
	    final int minBatchSize = 200;
		List<ConsumerRecord<String, String>> buffer = new ArrayList<>();
		
		 while (true) {
	         ConsumerRecords<String, String> records = kafkaConsumer.poll(100);
	         for (ConsumerRecord<String, String> record : records)
	             System.out.printf("offset = %d, key = %s, value = %s", record.offset(), record.key(), record.value());
	     }
		
        // close Kafka
//        kafkaConsumer.close();
		 */
		
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "test");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Arrays.asList("test"));
		
		
		BufferedWriter out = new BufferedWriter(new FileWriter("output.txt"));
		
		while (true) {
			try {
				System.out.println("hey");
				ConsumerRecords<String, String> records = consumer.poll(100);
				for (ConsumerRecord<String, String> record : records) {
					System.out.println(record.value());
					JSONParser parser = new JSONParser();
					JSONObject json = (JSONObject)parser.parse(record.value());
					
					JSONObject newJson = new JSONObject();
					
					newJson.put("lid", json.get("lid"));
					newJson.put("flw", json.get("app"));
					newJson.put("app", json.get("appNum"));
					newJson.put("s_date", json.get("s_date"));
					newJson.put("e_date", json.get("e_date"));

					
					out.write(newJson.toString());
					out.write("\n");
					out.flush();
					newJson.clear();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
