package org.apl.netbig.whiplash;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Properties;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apl.netbig.execute.MonitorMemory;
import org.apl.netbig.platformx.FlowSim;
import org.apl.netbig.platformx.VirtualPlatformX;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class AbnormalityDetector implements Runnable {
	// static variables
	private static ArrayList<JSONObject> whiteList = new ArrayList<JSONObject>();	// white list
	/* (example)
	[{"app":1,"flw":[1,2,3,4],"duration":[2,2,2]},
	{"app":2,"flw":[5,6,1,8],"duration":[3,3,3]}]
	 */
	
//	private static ConcurrentHashMap<Long, Long> maxWaitingTimeMap = new ConcurrentHashMap<Long, Long>();
	
	public static LinkedList<JSONObject> patternQueue = new LinkedList<JSONObject>();	// linked list of linked list (pattern map)
	private static ArrayList<Long> lidArray = new ArrayList<Long>();	// temporary array list of lid (matched sub pattern of white list)
	private String whiteListInputFileName;
	
	private static Object patternQueueLock = new Object();
	
	public AbnormalityDetector(String whiteListInputFileName) { // thread를 위한 생성자
		this.whiteListInputFileName = whiteListInputFileName;
	}
	
	public static boolean isFlwInWhiteList(long flwNum) { // 해당 flowNum이 whiteList에 존재하는지를 반환
		// whiteList 순회
		for(int i = 0; i < whiteList.size(); i++) {
			JSONArray flwList = (JSONArray)whiteList.get(i).get("flw"); // flw 리스트 가져옴
			for(int j = 0; j < flwList.size(); j++) { // flwList 순회
				if(flwNum == Long.parseLong(flwList.get(j).toString())) // 해당 flowNum이 whiteList에 존재하면 true 반환
					return true;
			}
		}
		return false; // 못찾으면 false 반환
	}
	
	public static boolean isLidInpatternQueue(long lid) { // 해당 lid가 patternQueue에 존재하는지를 반환
		synchronized(patternQueueLock) {
			if(!patternQueue.isEmpty()) {
			//	LinkedList<JSONObject> firstObjectOfpatternQueue = patternQueue.getFirst(); // patternQueue의 첫번째 줄 가져옴 (모든 플로우가 포함되어있기 때문)
				for(int i = 0; i < patternQueue.size(); i++) { // firstObjectOfpatternQueue 순회
					long patternQueueLid = Long.parseLong(patternQueue.get(i).get("lid").toString()); // lid 가져옴
					if(lid == patternQueueLid) // 해당 lid가 patternQueue에 존재하면 true 반환
						return true;
				}
			}
			return false; // 못찾으면 false 반환
		}
	}
	
	
    // find whiteListIdx in sub pattern (return : True/False)
	public static boolean patternSearch (LinkedList<JSONObject> subPattern, int whiteListIdx) {		
		lidArray.clear(); // lidArray initialize
		int i=0, j=0;
		int count=0;
		int taskFound=0;
		JSONObject whiteObject = whiteList.get(whiteListIdx);
		
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat formatterMS = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
        Date before_e_date = null;
		Date after_s_date = null;
		Date temp_e_date = null;
		
		long subPatternDuration = 0;
		long whitePatternDuration = 0;
		boolean isFirst = true;
	
		// whitelist의 각 요소를 가져옴(parsing)
		long appID = Long.parseLong(whiteObject.get("app").toString());
		JSONArray flwList = (JSONArray)whiteObject.get("flw"); // flw 리스트 가져옴
		JSONArray durationList = (JSONArray)whiteObject.get("duration"); // duration 리스트 가져옴

		for (; i < flwList.size(); i++) { // flow 리스트 순회
			if(i==0) isFirst = true;
			else if(isFirst == true) break;
			
			long flwNum = Long.parseLong(flwList.get(i).toString());
			
			for(; j<subPattern.size(); j++) {
				count++;
				
				if(isFirst == false) {
					whitePatternDuration = Long.parseLong(durationList.get(i-1).toString());
				} else {
					subPatternDuration = 0;
					whitePatternDuration = 0;
				}
				long spflw = Long.parseLong(subPattern.get(j).get("flw").toString());
				long wpflw = flwNum;//Long.toString(flwNum);
				
				// flw & duration 판단
				if(spflw == wpflw && isFirst == true || 
						spflw == wpflw &&
						(whitePatternDuration - subPatternDuration) <= 1*1000 &&
						(whitePatternDuration - subPatternDuration) >= -1*1000) {
					
					// 해당 app의 log에 존재하는지 확인
					if(VirtualPlatformX.matchingPlatformLogWithFlow(appID, Long.parseLong(subPattern.get(j).get("lid").toString()))) {
						
						try {						
							long spflwLid = Long.parseLong(subPattern.get(j).get("lid").toString());
							lidArray.add(spflwLid);

							temp_e_date = formatterMS.parse(subPattern.get(j).get("e_date").toString());

							j=count;
							taskFound++;
							isFirst = false;	
							break;
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} // end of if
			} // end of for(; j<subPattern.size(); j++)
			
			if(taskFound == flwList.size()) { // 패턴을 찾으면
				if(VirtualPlatformX.matchingPlatformLog(appID, lidArray)) { // 플랫폼에 물어봄
					
					// (04.10) 비정상 패턴 소요시간 측정을 위한 코드
					int size = lidArray.size();
					for(int k=0; k<size; k++) {
						Date afterTimeStamp = new Date();
						Date beforeTimeStamp = (Date) MonitorMemory.timeStampMap.get(lidArray.get(k)).clone();
						long requiredTime = (long)afterTimeStamp.getTime() - (long)beforeTimeStamp.getTime();
						synchronized(MonitorMemory.normalTimeCalculationLock) {
							MonitorMemory.totalOfNormalRequiredTime += requiredTime;
							MonitorMemory.numOfNormalComplete++;
						}
					}
					
					return true;
				} else {
					return false;
				}
				
			} // end of if
			
		} // end of for(; i<whitePattern.size(); i++)
		
		return false;
		
	} // end of function 'patternSearch'		
	
	
	// remove flows in subPattern (result = subItr)
	public static void subPatternRemoval(ListIterator subItr) {
		while(subItr.hasNext()) {
			JSONObject flw = (JSONObject) subItr.next();				
			long flwLid = Long.parseLong(flw.get("lid").toString());
			
			// remove all sub pattern in patternQueue
			for(int i=0; i<lidArray.size(); i++) {
				if(flwLid == lidArray.get(i)) {
					subItr.remove();
					break;
				}
			}
		}
	}
	
	
	// remove whiteSubPattern in patternQueue (result = patternQueue)
	public static void PatternRemoval() {
		
		synchronized(patternQueueLock) {
			subPatternRemoval(patternQueue.listIterator());
		}
	}
	
	public static void removeFlowFrompatternQueue(long lid) { // patternQueue에서 해당 lid를 제거
		synchronized(patternQueueLock) {
			ListIterator itr = patternQueue.listIterator();	// patternQueue의 iterator	
			while(itr.hasNext()) { // patternQueue의 한 줄 순회
				JSONObject flw = (JSONObject) itr.next();				
				long flwLid = Long.parseLong(flw.get("lid").toString());
				
				if(flwLid == lid) { // 해당 lid 찾으면
					itr.remove(); // 제거
					break;
				}
			}
		}
	}
	
	
	// find abnormal pattern in flowList (result = patternQueue)
	public static void findAbnormalPattern(JSONObject flw) {
	//	System.out.println("> Algorithm Starts!");
		
		int whiteListSize = whiteList.size();
		
	        // make pattern map
		synchronized(patternQueueLock) {
			patternQueue.add(flw);
		}	
				
		for(int j = 0; j < whiteListSize; j++) {
			if(patternQueue.isEmpty()) break;
			else {
				if(patternSearch(patternQueue, j) == true) { // normal pattern found!
					PatternRemoval(); // remove pattern in pattern map
				}else {
					continue;
				}
			}
		} // end of for(j=0; j<whiteListSize; j++)
	}
	
	public static void realTimeAbnormalityDetector(String whiteListInputFileName) throws ParseException {
		
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "test");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Arrays.asList("test"));
		
		SimpleDateFormat formatterMs = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		JSONParser jsonParser = new JSONParser();
		FileReader patternReader = null;
		BufferedReader bufferedpatternReader = null;
		
		try {
			// white list file loading
			patternReader = new FileReader(whiteListInputFileName);
			bufferedpatternReader = new BufferedReader(patternReader);

			String whiteListLine = null;
			
            // make whiteList
			while(true) {
				whiteListLine = bufferedpatternReader.readLine();
				if(whiteListLine == null) break;
				
				JSONObject whiteObject = (JSONObject)jsonParser.parse(whiteListLine); // 파일 한 줄 parsing
				whiteList.add(whiteObject);
			//	CalculateMaxWaitingTime(whiteObject);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}	
		
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(100);
			for (ConsumerRecord<String, String> record : records) {
			//	System.out.println(record.value());
			//	JSONParser parser = new JSONParser();
				JSONObject json = null;
				try {
					json = (JSONObject)jsonParser.parse(record.value());
				} catch (org.json.simple.parser.ParseException e1) {
					e1.printStackTrace();
				}
									
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("lid", json.get("lid"));
				jsonObject.put("flw", json.get("flw"));
				jsonObject.put("app", json.get("app"));
				jsonObject.put("s_date", json.get("s_date"));
				jsonObject.put("e_date", json.get("e_date"));
		//		System.err.println("[MatchingEngine] INPUT : " + jsonObject);
				
				long flwNum = Long.parseLong(jsonObject.get("flw").toString());
				long flwLid = Long.parseLong(jsonObject.get("lid").toString());
				
				if(!isFlwInWhiteList(flwNum)) {
					System.out.println("[AbnormalityDetector] RETURN : " + flwLid + " is abnormal(X).");
					if(!FlowSim.abnormalMap.containsKey(flwLid)) {
						System.err.println("정상을 비정상이라고 판단. lid: " + flwLid);
						synchronized(MonitorMemory.wrongLock) {
							MonitorMemory.numOfWrongAnswer++;
						}
					}
					FlowSim.abnormalMap.remove(flwLid);
				} else {
					// apply pattern finding algorithm (return = patternQueue)
					MonitorMemory.timeStampMap.put(flwLid, new Date());
					findAbnormalPattern(jsonObject);
					
					Timer removeAbnormalTimer = new Timer();
					removeAbnormalTimer.schedule(new TaskOfRemoveAbnormal(jsonObject), 10*1000);
				}
			}
		}
	}
	
	@Override
	public void run() {
		try {
			this.realTimeAbnormalityDetector(whiteListInputFileName);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void removeAbnormalPattern(JSONObject flowObject) { // 주기적으로 타임아웃된 플로우를 지움
		// 이 flow가 아직 map에 남아있는지 확인
		long lid = Long.parseLong(flowObject.get("lid").toString()); // lid 가져옴
		if(isLidInpatternQueue(lid)) { // lid가 patternQueue에 존재하면
			
			// 비정상 패턴 소요시간 측정을 위한 코드.
			Date afterTimeStamp = new Date();
			Date beforeTimeStamp = (Date) MonitorMemory.timeStampMap.get(lid).clone();
			long requiredTime = (long)afterTimeStamp.getTime() - (long)beforeTimeStamp.getTime();
		//	System.out.println("lid: " + lid + ", requiredTime: " + requiredTime);
			synchronized(MonitorMemory.abnormalTimeCalculationLock) {
				MonitorMemory.totalOfAbnormalRequiredTime += requiredTime;
				MonitorMemory.numOfAbnormalComplete++;
			}
			
			removeFlowFrompatternQueue(lid); // patternQueue 에서 해당 lid 제거
			if(!FlowSim.abnormalMap.containsKey(lid)) {
				System.err.println("정상을 비정상이라고 판단. lid: " + lid);
				synchronized(MonitorMemory.wrongLock) {
					MonitorMemory.numOfWrongAnswer++;
				}
			}
			FlowSim.abnormalMap.remove(lid);
			System.out.println("Lid " + lid + " is abnormal(X). (removed by timeout)");
		}//	else
		//	System.out.println("Lid " + lid + " is already removed. ");
	}
	/*
	public static void CalculateMaxWaitingTime(JSONObject whitePattern) {
				
		JSONArray flwList = (JSONArray)whitePattern.get("flw");
		JSONArray durationList = (JSONArray)whitePattern.get("duration");
		
		long duration = 0;
		
		for(int i=flwList.size(); i>0; i--) {
			long flw = Long.parseLong(flwList.get(i-1).toString());
			if(i != flwList.size()) {
				duration += Long.parseLong(durationList.get(i-1).toString());
			}
			if(maxWaitingTimeMap.containsKey(flw)) {
				if(duration > maxWaitingTimeMap.get(flw)) {
					maxWaitingTimeMap.put(flw, duration);
				}
			}else {
				maxWaitingTimeMap.put(flw, duration);
			}
		}
	}*/
}
