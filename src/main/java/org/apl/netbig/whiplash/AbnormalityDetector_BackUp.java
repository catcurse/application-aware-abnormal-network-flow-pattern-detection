package org.apl.netbig.whiplash;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Properties;
import java.util.Timer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apl.netbig.execute.MonitorMemory;
import org.apl.netbig.platformx.FlowSim;
import org.apl.netbig.platformx.VirtualPlatformX;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class AbnormalityDetector_BackUp implements Runnable {
	// static variables
	private static ArrayList<JSONObject> whiteList = new ArrayList<JSONObject>();	// white list
	/* (example)
	[{"app":1,"flw":[1,2,3,4],"duration":[2,2,2]},
	{"app":2,"flw":[5,6,1,8],"duration":[3,3,3]}]
	 */
	public static LinkedList<LinkedList<JSONObject>> patternMap = new LinkedList<LinkedList<JSONObject>>();	// linked list of linked list (pattern map)
	private static ArrayList<Long> lidArray = new ArrayList<Long>();	// temporary array list of lid (matched sub pattern of white list)
	private String whiteListInputFileName;
	
	private static Object patternMapLock = new Object();
	
	public AbnormalityDetector_BackUp(String whiteListInputFileName) { // thread를 위한 생성자
		this.whiteListInputFileName = whiteListInputFileName;
	}
	
	public static boolean isFlwInWhiteList(long flwNum) { // 해당 flowNum이 whiteList에 존재하는지를 반환
		// whiteList 순회
		for(int i = 0; i < whiteList.size(); i++) {
			JSONArray flwList = (JSONArray)whiteList.get(i).get("flw"); // flw 리스트 가져옴
			for(int j = 0; j < flwList.size(); j++) { // flwList 순회
				if(flwNum == Long.parseLong(flwList.get(j).toString())) // 해당 flowNum이 whiteList에 존재하면 true 반환
					return true;
			}
		}
		return false; // 못찾으면 false 반환
	}
	
	public static boolean isLidInPatternMap(long lid) { // 해당 lid가 patternMap에 존재하는지를 반환
		synchronized(patternMapLock) {
			if(!patternMap.isEmpty()) {
				LinkedList<JSONObject> firstObjectOfPatternMap = patternMap.getFirst(); // patternMap의 첫번째 줄 가져옴 (모든 플로우가 포함되어있기 때문)
				for(int i = 0; i < firstObjectOfPatternMap.size(); i++) { // firstObjectOfPatternMap 순회
					long patternMapLid = Long.parseLong(firstObjectOfPatternMap.get(i).get("lid").toString()); // lid 가져옴
					if(lid == patternMapLid) // 해당 lid가 patternMap에 존재하면 true 반환
						return true;
				}
			}
			return false; // 못찾으면 false 반환
		}
	}
	
	
    // find whiteListIdx in sub pattern (return : True/False)
	public static boolean patternSearch (LinkedList<JSONObject> subPattern, int whiteListIdx) {		
		lidArray.clear(); // lidArray initialize
		int i=0, j=0;
		int count=0;
		int taskFound=0;
		JSONObject whiteObject = whiteList.get(whiteListIdx);
		
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat formatterMS = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
        Date before_e_date = null;
		Date after_s_date = null;
		Date temp_e_date = null;
		
		long subPatternDuration = 0;
		long whitePatternDuration = 0;
		boolean isFirst = true;
	
		// whitelist의 각 요소를 가져옴(parsing)
		long appID = Long.parseLong(whiteObject.get("app").toString());
		JSONArray flwList = (JSONArray)whiteObject.get("flw"); // flw 리스트 가져옴
		JSONArray durationList = (JSONArray)whiteObject.get("duration"); // duration 리스트 가져옴

		for (; i < flwList.size(); i++) { // flow 리스트 순회
			if(i==0) isFirst = true;
			else if(isFirst == true) break;
			
			long flwNum = Long.parseLong(flwList.get(i).toString());
			
			for(; j<subPattern.size(); j++) {
				count++;
				
				if(isFirst == false) {
					whitePatternDuration = Long.parseLong(durationList.get(i-1).toString());
				} else {
					subPatternDuration = 0;
					whitePatternDuration = 0;
				}
				long spflw = Long.parseLong(subPattern.get(j).get("flw").toString());
				long wpflw = flwNum;//Long.toString(flwNum);
				
				// flw & duration 판단
				if(spflw == wpflw && isFirst == true || 
						spflw == wpflw &&
						(whitePatternDuration - subPatternDuration) <= 1*1000 &&
						(whitePatternDuration - subPatternDuration) >= -1*1000) {
					
					// 해당 app의 log에 존재하는지 확인
					if(VirtualPlatformX.matchingPlatformLogWithFlow(appID, Long.parseLong(subPattern.get(j).get("lid").toString()))) {
						
						try {						
							long spflwLid = Long.parseLong(subPattern.get(j).get("lid").toString());
							lidArray.add(spflwLid);

							temp_e_date = formatterMS.parse(subPattern.get(j).get("e_date").toString());

							j=count;
							taskFound++;
							isFirst = false;	
							break;
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} // end of if
			} // end of for(; j<subPattern.size(); j++)
			
			if(taskFound == flwList.size()) { // 패턴을 찾으면
				if(VirtualPlatformX.matchingPlatformLog(appID, lidArray)) { // 플랫폼에 물어봄
					
					// (04.10) 비정상 패턴 소요시간 측정을 위한 코드
					int size = lidArray.size();
					for(int k=0; k<size; k++) {
						Date afterTimeStamp = new Date();
						Date beforeTimeStamp = MonitorMemory.timeStampMap.get(lidArray.get(k));
						long requiredTime = (long)afterTimeStamp.getTime() - (long)beforeTimeStamp.getTime();
						synchronized(MonitorMemory.normalTimeCalculationLock) {
							MonitorMemory.totalOfNormalRequiredTime += requiredTime;
							MonitorMemory.numOfNormalComplete++;
						}
					}
					
				//	System.out.println("lid: " + lidArray);
				//	System.out.println("\n>> Pattern " + (whiteListIdx+1) + " found!");
					return true;
				} else {
				//	System.out.println("lid: " + lidArray);
				//	System.out.println("\n>> Not in Log (PlatformX)");
					return false;
				}
				
			} // end of if
			
		} // end of for(; i<whitePattern.size(); i++)
		
	//	System.out.println(">> No pattern found!");
		return false;
		
	} // end of function 'patternSearch'		
	
	
	// remove flows in subPattern (result = subItr)
	public static void subPatternRemoval(ListIterator subItr) {
		while(subItr.hasNext()) {
			JSONObject flw = (JSONObject) subItr.next();				
			long flwLid = Long.parseLong(flw.get("lid").toString());
			
			// remove all sub pattern in patternMap
			for(int i=0; i<lidArray.size(); i++) {
				if(flwLid == lidArray.get(i)) {
					subItr.remove();
					break;
				}
			}
		}
	}
	
	
	// remove whiteSubPattern in patternMap (result = patternMap)
	public static void PatternRemoval() {
		
		synchronized(patternMapLock) {
			ListIterator itr = patternMap.listIterator();		
					
			while(itr.hasNext()) {
				LinkedList<JSONObject> subPatternMap = (LinkedList<JSONObject>) itr.next();	
				long spm_0 = Long.parseLong(subPatternMap.get(0).get("lid").toString());
				
				// remove 1st list matching white list in patternMap
				if(spm_0 == lidArray.get(0)) {
					itr.remove();
					continue;
				}
				
				// remove all sub pattern in patternMap
				ListIterator subItr = subPatternMap.listIterator();
				subPatternRemoval(subItr);
			}
		
			// remove list with no remain flows and redundant list in patternMap 
			itr = patternMap.listIterator();
			int beforeMapSize = 0;
			while(itr.hasNext()) {
				LinkedList<JSONObject> subPatternMap = (LinkedList<JSONObject>) itr.next();
				if(subPatternMap.size() == 0) itr.remove();
				
				else if(subPatternMap.size() == beforeMapSize) {
					itr.remove();
				}
				beforeMapSize = subPatternMap.size();
			}
		
			
			// print after remove normal pattern in patternMap
			itr = patternMap.listIterator();
			int count = 0;
			while(itr.hasNext()) {
				JSONArray flwList = new JSONArray();
				JSONArray newFlwList = new JSONArray();
				JSONParser jsonParser = new JSONParser();
				
				try {
					flwList = (JSONArray)jsonParser.parse(itr.next().toString());
				} catch (org.json.simple.parser.ParseException e) {
					e.printStackTrace();
				}
				
				for(int i=0; i<flwList.size(); i++) {
					JSONObject flw = (JSONObject) flwList.get(i);
					flw.remove("s_date");
					flw.remove("e_date");
					newFlwList.add(flw);
				}
				
			//	System.out.print(count + "] len=");
			//	System.out.println(newFlwList.size() + " " + newFlwList);
				count++;
			}
		}
	}
	
	public static void removeFlowFromPatternMap(long lid) { // PatternMap에서 해당 lid를 제거
		synchronized(patternMapLock) {
			ListIterator itr = patternMap.listIterator();	// patternMap의 iterator	
			
			while(itr.hasNext()) { // patternMap 순회
				LinkedList<JSONObject> subPatternMap = (LinkedList<JSONObject>) itr.next();	
				ListIterator subItr = subPatternMap.listIterator(); // patternMap의 한 줄의 iterator
				
				while(subItr.hasNext()) { // patternMap의 한 줄 순회
					JSONObject flw = (JSONObject) subItr.next();				
					long flwLid = Long.parseLong(flw.get("lid").toString());
					
					if(flwLid == lid) { // 해당 lid 찾으면
						subItr.remove(); // 제거
						break;
					}
				}
			}
		}
	}
	
	
	// find abnormal pattern in flowList (result = patternMap)
	public static void findAbnormalPattern(JSONObject flw) {
	//	System.out.println("> Algorithm Starts!");
		
		int whiteListSize = whiteList.size();
		
	        // make pattern map
		synchronized(patternMapLock) {
			for(LinkedList<JSONObject> sp : patternMap) {
				sp.add(flw);
			} // end of for(LinkedList<JSONObject> sp : patternMap)
		}	
		LinkedList<JSONObject> subPattern = new LinkedList<JSONObject>();
		subPattern.add(flw);
		
		synchronized(patternMapLock) {
			patternMap.add(subPattern);
		}
	//	System.out.println("\n> New flow " + flow.get("flw") + " added to patternMap");
		
	//	for(int printIndex=0; printIndex<patternMap.size(); printIndex++) {
		//	System.out.print(printIndex + ") len=");
		//	System.out.print(patternMap.get(printIndex).size() + " ");
		//	System.out.println(patternMap.get(printIndex));
	//	}
				
		for(int j = 0; j < whiteListSize; j++) {
			if(patternMap.isEmpty()) break;
			else {
				if(patternSearch(patternMap.getFirst(), j) == true) { // normal pattern found!
					PatternRemoval(); // remove pattern in pattern map
				}else {
					continue;
				}
			}
		} // end of for(j=0; j<whiteListSize; j++)
	}
	
	public static void realTimeAbnormalityDetector(String whiteListInputFileName) throws ParseException {
		
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "test");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Arrays.asList("test"));
		
		SimpleDateFormat formatterMs = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		JSONParser jsonParser = new JSONParser();
		FileReader patternReader = null;
		BufferedReader bufferedpatternReader = null;
		
		try {
			// white list file loading
			patternReader = new FileReader(whiteListInputFileName);
			bufferedpatternReader = new BufferedReader(patternReader);

			String whiteListLine = null;
			
            // make whiteList
			while(true) {
				whiteListLine = bufferedpatternReader.readLine();
				if(whiteListLine == null) break;
				
				JSONObject whiteObject = (JSONObject)jsonParser.parse(whiteListLine); // 파일 한 줄 parsing
				whiteList.add(whiteObject);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}	
		
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(100);
			for (ConsumerRecord<String, String> record : records) {
			//	System.out.println(record.value());
			//	JSONParser parser = new JSONParser();
				JSONObject json = null;
				try {
					json = (JSONObject)jsonParser.parse(record.value());
				} catch (org.json.simple.parser.ParseException e1) {
					e1.printStackTrace();
				}
									
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("lid", json.get("lid"));
				jsonObject.put("flw", json.get("flw"));
				jsonObject.put("app", json.get("app"));
				jsonObject.put("s_date", json.get("s_date"));
				jsonObject.put("e_date", json.get("e_date"));
		//		System.err.println("[MatchingEngine] INPUT : " + jsonObject);
				
				long flwNum = Long.parseLong(jsonObject.get("flw").toString());
				long flwLid = Long.parseLong(jsonObject.get("lid").toString());
				
				if(!isFlwInWhiteList(flwNum)) {
					System.out.println("[AbnormalityDetector] RETURN : " + flwLid + " is abnormal(X).");
					if(!FlowSim.abnormalMap.containsKey(flwLid)) System.err.println("망함. " + flwLid);
					FlowSim.abnormalMap.remove(flwLid);
				} else {
					// apply pattern finding algorithm (return = patternMap)
					MonitorMemory.timeStampMap.put(flwLid, new Date());
					
					findAbnormalPattern(jsonObject);
					
					// finally, find remain flows
				//	System.out.println(">\tAbnormal Pattern : ");

					synchronized(patternMapLock) {
						ListIterator itr = patternMap.listIterator();		
						int count = 0;
						
						while(itr.hasNext()) {
							JSONArray flwList = new JSONArray();
							JSONArray newFlwList = new JSONArray();
							
							try {
								flwList = (JSONArray)jsonParser.parse(itr.next().toString());
							} catch (org.json.simple.parser.ParseException e) {
								e.printStackTrace();
							}
							
							for(int i=0; i<flwList.size(); i++) {
								JSONObject flw = (JSONObject) flwList.get(i);
								flw.remove("s_date");
								flw.remove("e_date");
								newFlwList.add(flw);
							}
							
						//	System.out.print(count + "] len=");
						//	System.out.println(newFlwList.size() + " " + newFlwList);
						//	System.err.println(newFlwList);
							count++;
							
						}
					}
				}
				
				Timer removeAbnormalTimer = new Timer();
				removeAbnormalTimer.schedule(new TaskOfRemoveAbnormal(jsonObject), 10*1000); // 10초 후에 한번 실행
			}
		}
	}
	
	@Override
	public void run() {
		try {
			this.realTimeAbnormalityDetector(whiteListInputFileName);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void removeAbnormalPattern(JSONObject flowObject) { // 주기적으로 타임아웃된 플로우를 지움
		// 이 flow가 아직 map에 남아있는지 확인
		long lid = Long.parseLong(flowObject.get("lid").toString()); // lid 가져옴
		if(isLidInPatternMap(lid)) { // lid가 patternMap에 존재하면
			
			// 비정상 패턴 소요시간 측정을 위한 코드.
			Date afterTimeStamp = new Date();
			Date beforeTimeStamp = MonitorMemory.timeStampMap.get(lid);
			long requiredTime = (long)afterTimeStamp.getTime() - (long)beforeTimeStamp.getTime();
		//	System.out.println("lid: " + lid + ", requiredTime: " + requiredTime);
			synchronized(MonitorMemory.abnormalTimeCalculationLock) {
				MonitorMemory.totalOfAbnormalRequiredTime += requiredTime;
				MonitorMemory.numOfAbnormalComplete++;
			}
			
			removeFlowFromPatternMap(lid); // patternMap 에서 해당 lid 제거
			if(!FlowSim.abnormalMap.containsKey(lid)) System.err.println("망함. " + lid);
			FlowSim.abnormalMap.remove(lid);
			System.out.println("Lid " + lid + " is abnormal(X). (removed by timeout)");
		}//	else
		//	System.out.println("Lid " + lid + " is already removed. ");
	}
}
