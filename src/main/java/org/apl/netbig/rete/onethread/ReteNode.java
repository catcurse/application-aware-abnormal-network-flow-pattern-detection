package org.apl.netbig.rete.onethread;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

import org.apl.netbig.execute.MonitorMemory;
import org.apl.netbig.platformx.FlowSim;
import org.apl.netbig.platformx.VirtualPlatformX;

public class ReteNode {
	
	private long nodeID;
	private ReteNode firstParentReteNode = null;
	private ReteNode secondParentReteNode = null;
	private boolean isLeafNode = false;
	private long profiledDuration;
	private long errorRange;
	private long appID;
	
	private long maxWaitingTime;
	
//	private long localInstanceID = -1;
//	private boolean isRunCheckTask = false;
//	private boolean isRunRemoveTask = false;
	//VirtualPlatformX platformX = new VirtualPlatformX();
		
	
//	private synchronized long genLocalInstanceID() {
	/*	if(this.stateTable.isEmpty() && this.localInstanceID != -1) { // state table이 비워져 있으면 instance ID 초기화
			this.localInstanceID = -1;
		}*/
//		this.localInstanceID++;
//		return this.localInstanceID;
//	}
	
	private Object stateTableLock = new Object();
//	private Object waitListLock = new Object();
	private Object instanceMapLock = new Object();
	
	private ConcurrentHashMap<Long, ReteNode> childReteNodeMap = new ConcurrentHashMap<Long, ReteNode>();
	private ConcurrentHashMap<Long, Integer> childBoolMap = new ConcurrentHashMap<Long, Integer>();
	private ConcurrentHashMap<Long, NodeInstance> waitedInstanceList = new ConcurrentHashMap<Long, NodeInstance>();
	
	private ConcurrentHashMap<Long, ConcurrentHashMap<Long, Integer>> stateTable = new ConcurrentHashMap<Long, ConcurrentHashMap<Long, Integer>>();
	private ConcurrentHashMap<Long, NodeInstance> myInstanceMap = new ConcurrentHashMap<Long, NodeInstance>();
	
	public ReteNode(long nodeID, ReteNode firstParentReteNode, ReteNode secondParentReteNode, boolean isLeafNode, long duration, long errorRange) {
		this.nodeID = nodeID;
		this.firstParentReteNode = firstParentReteNode;
		this.secondParentReteNode = secondParentReteNode;
		this.isLeafNode = isLeafNode;
		this.profiledDuration = duration;
		this.errorRange = errorRange;
		this.appID = -1;
	}
	
	public void trigger(Date s_date, Date e_date, long lid) {
		if(this.isAlphaNode()) {
			ConcurrentHashMap<Long, Integer> InstanceOfChildBoolMap = new ConcurrentHashMap<Long, Integer>(this.childBoolMap);
			
			// date 정보 저장.
			ArrayList<Date> dateInfo = new ArrayList<Date>();
			dateInfo.add(s_date);
			dateInfo.add(e_date);
			ConcurrentHashMap<Long, ArrayList<Date>> newFlwStack = new ConcurrentHashMap<Long, ArrayList<Date>>();
			newFlwStack.put(this.nodeID, dateInfo);
			
			// lid 정보 저장.
			ArrayList<Long> newLidStack = new ArrayList<Long>();
			newLidStack.add(lid);
			
			long newLocalInstanceID;
			NodeInstance newInstance;
			
			newLocalInstanceID = MatchingEngine.genInstanceID();//this.genLocalInstanceID();
			newInstance = new NodeInstance(newLocalInstanceID, this.nodeID, newFlwStack, -1, -1, newLidStack, this.getNumberOfChild(), this.childBoolMap); // 새로운 인스턴스 생성.
			synchronized(this.stateTableLock) {
				this.stateTable.put(newLocalInstanceID, InstanceOfChildBoolMap);
			}
		//	this.stateTableTimeMap.put(newLocalInstanceID, new Date());
			synchronized(this.instanceMapLock) {
				this.myInstanceMap.put(newLocalInstanceID, newInstance);
			}
				
			// map iterator
			Iterator<Long> keys = this.childReteNodeMap.keySet().iterator();
			while(keys.hasNext()) {
				long key = keys.next();
				this.childReteNodeMap.get(key).nextTrigger(this.nodeID, newLocalInstanceID, newInstance); // 모든 next node들에게 쏴줌.
			}
			
		}else {
			//System.err.println(this.getNodeID() + "this node is not alpha node.(trigger method)");
		}
	}
	
	public void nextTrigger(long recvNodeID, long recvInstanceID, NodeInstance recvInstance) {
		if(recvNodeID == this.firstParentReteNode.getNodeID()) { // 새로 들어온 플로우가 first parent인 경우.
			this.waitedInstanceList.put(recvInstance.getInstanceID(), recvInstance);
		}else { // 새로 들어온 플로우가 second parent인 경우.
			
			if(this.waitedInstanceList.isEmpty()) { // waitedInstanceList이 비어있는경우, match not found
				this.secondParentReteNode.setStateToFalse(recvInstanceID, this.nodeID);
			}else {			
				boolean matchingOffset = false;
				Iterator<Long> waitedInstanceItr;
				
				if(this.isLeafNode) { // leaf node인 경우.
					ConcurrentHashMap<Long, NodeInstance> cloneOfWaitList = new ConcurrentHashMap<Long, NodeInstance>(this.waitedInstanceList);
					
				//	synchronized(this.waitListLock) {
						waitedInstanceItr = cloneOfWaitList.keySet().iterator();
						while(waitedInstanceItr.hasNext()) {
							NodeInstance waitedInstance = cloneOfWaitList.get(waitedInstanceItr.next());
							
							Date s_dateOfSecond = recvInstance.getFlwStack().get(recvNodeID).get(0);
							Date e_dateOfFirst = null;
							if(this.firstParentReteNode.isAlphaNode()) {
								e_dateOfFirst = waitedInstance.getFlwStack().get(this.firstParentReteNode.getNodeID()).get(1);
							}else {
								e_dateOfFirst = waitedInstance.getFlwStack().get(this.firstParentReteNode.secondParentReteNode.getNodeID()).get(1);
							}
							long observedDuration = (long) (s_dateOfSecond.getTime() - e_dateOfFirst.getTime()); // second의 s_date - first의 e_date
							
							if((observedDuration-this.profiledDuration) <= this.errorRange && (observedDuration-this.profiledDuration) >= -(this.errorRange)) {
							// 듀레이션에 맞는 패턴을 찾은경우.

								ArrayList<Long> lidStack = new ArrayList<Long>(waitedInstance.getLidStack());
								lidStack.addAll(recvInstance.getLidStack());
								
							//	System.out.println("lidStack: " + lidStack.toString());
								
								if(VirtualPlatformX.matchingPlatformLog(this.appID, lidStack)) { // 플랫폼에 물어봐서 일치한 경우.
								//	System.out.println("asdf: " + this.getNodeID() + ", " + observedDuration + ", " + this.profiledDuration);

									// (04.10) 정상 패턴 소요시간 측정을 위한 코드 추가
									int size = lidStack.size();
									for(int i=0; i<size; i++) {
										Date afterTimeStamp = new Date();
										Date beforeTimeStamp = (Date) MonitorMemory.timeStampMap.get(lidStack.get(i)).clone();
										long requiredTime = (long)afterTimeStamp.getTime() - (long)beforeTimeStamp.getTime();
										MonitorMemory.timeStampMap.remove(lidStack.get(i));
										synchronized(MonitorMemory.normalTimeCalculationLock) {
											MonitorMemory.totalOfNormalRequiredTime += requiredTime;
											MonitorMemory.numOfNormalComplete++;
										}
									}
									
									matchingOffset = true;
									this.setStateToParents(waitedInstance.getInstanceID(), recvInstance.getInstanceID(), 1);
								//	synchronized(this.waitListLock) {
										this.waitedInstanceList.remove(waitedInstance.getInstanceID());
								//	}
								
									break;
								}
							}
						}
				//	}
					
					if(!matchingOffset) {
						this.secondParentReteNode.setStateToFalse(recvInstanceID, this.nodeID); // 듀레이션에 맞는 경우가 없으면 second parent의 state table에 0을 반환.						
					}
					
					
				}else { // agg node인 경우.
					
					
				//	synchronized(this.waitListLock) {
						ConcurrentHashMap<Long, NodeInstance> cloneOfWaitList = new ConcurrentHashMap<Long, NodeInstance>(this.waitedInstanceList);
						waitedInstanceItr = cloneOfWaitList.keySet().iterator();
						while(waitedInstanceItr.hasNext()) {
							NodeInstance waitedInstance = cloneOfWaitList.get(waitedInstanceItr.next());
			
							Date s_dateOfSecond = recvInstance.getFlwStack().get(recvNodeID).get(0);
							Date e_dateOfFirst = null;
							if(this.firstParentReteNode.isAlphaNode()) {
								e_dateOfFirst = waitedInstance.getFlwStack().get(this.firstParentReteNode.getNodeID()).get(1);
							}else {
								e_dateOfFirst = waitedInstance.getFlwStack().get(this.firstParentReteNode.secondParentReteNode.getNodeID()).get(1);
							}
							
							long observedDuration = (long) (s_dateOfSecond.getTime() - e_dateOfFirst.getTime()); // second의 s_date - first의 e_date
							
							if((observedDuration-this.profiledDuration) <= this.errorRange && (observedDuration-this.profiledDuration) >= -(this.errorRange)) {
								// 듀레이션에 맞는 패턴을 찾은경우.
							//	System.out.println(waitedInstance.getLidStack() + ", " + recvInstance.getLidStack() + ", node id: " + this.getNodeID() + ", " + observedDuration + ", " + this.profiledDuration);

								matchingOffset = true;
								
								ConcurrentHashMap<Long, Integer> InstanceOfChildBoolMap = new ConcurrentHashMap<Long, Integer>(this.childBoolMap);
								ConcurrentHashMap<Long, ArrayList<Date>> newFlwStack = new ConcurrentHashMap<Long, ArrayList<Date>>(waitedInstance.getFlwStack());
								newFlwStack.putAll(recvInstance.getFlwStack());
								
								ArrayList<Long> newLidStack = new ArrayList<Long>(waitedInstance.getLidStack());
								newLidStack.addAll(recvInstance.getLidStack());
								
								long newLocalInstanceID = MatchingEngine.genInstanceID();//this.genLocalInstanceID();
								NodeInstance newInstance = new NodeInstance(newLocalInstanceID, this.nodeID, newFlwStack, waitedInstance.getInstanceID(), recvInstanceID, newLidStack, this.getNumberOfChild(), this.childBoolMap); // 새로운 인스턴스 생성.
								synchronized(this.stateTableLock) {
									this.stateTable.put(newLocalInstanceID, InstanceOfChildBoolMap);
								}
								synchronized(this.instanceMapLock) {
									this.myInstanceMap.put(newLocalInstanceID, newInstance);
								}
								
								// 부모의 상태를 임시로 바꿔줌.
								waitedInstance.addNumberOfWaitFromChild(this.nodeID);
								recvInstance.addNumberOfWaitFromChild(this.nodeID);
								this.setStateToParents(waitedInstance.getInstanceID(), recvInstanceID, 2);
							
								// map iterator(모든 자식노드들에게 쏴줌)
								Iterator<Long> keys = this.childReteNodeMap.keySet().iterator();
								while(keys.hasNext()) {
									long key = keys.next();
									this.childReteNodeMap.get(key).nextTrigger(this.nodeID, newLocalInstanceID, newInstance); // 모든 next node들에게 쏴줌.
								}
							}
						}
				//	}
					
					if(!matchingOffset) {
						// 듀레이션에 맞는 경우가 없으면 second parent node에 바로 false 반환.
						this.secondParentReteNode.setStateToFalse(recvInstanceID, this.nodeID);
					}
				}
			}
		}
	}
	
	public void setStateToFalse(long recvInstanceID, long recvNodeID) {
	//	if(this.stateTable.containsKey(recvInstanceID))
		synchronized(this.stateTableLock) {
			if(this.stateTable.get(recvInstanceID).get(recvNodeID) != 1)
				this.stateTable.get(recvInstanceID).put(recvNodeID, 0);
			this.myInstanceMap.get(recvInstanceID).reduceNumberOfWaitFromChild();
		}
	}
	
	public void setStateToTrue(long recvInstanceID, long recvNodeID) {
	//	if(this.stateTable.containsKey(recvInstanceID)) 
		synchronized(this.stateTableLock) {
			this.stateTable.get(recvInstanceID).put(recvNodeID, 1);
			this.myInstanceMap.get(recvInstanceID).reduceNumberOfWaitFromChild();
		}
	}
	
	public void setStateToTemp(long recvInstanceID, long recvNodeID) {
	//	if(this.stateTable.containsKey(recvInstanceID))
		synchronized(this.stateTableLock) {
			if(this.stateTable.get(recvInstanceID).get(recvNodeID) == -1)
				this.stateTable.get(recvInstanceID).put(recvNodeID, 2);
		}
	}
	
	public void setStateToParents(long firstParentInstanceID, long secondParentInstanceID, int state) {
		
		if(state == 1) {
			this.firstParentReteNode.setStateToTrue(firstParentInstanceID, this.nodeID);
			this.secondParentReteNode.setStateToTrue(secondParentInstanceID, this.nodeID);
		}else if(state == 0) {
			this.firstParentReteNode.setStateToFalse(firstParentInstanceID, this.nodeID);
			this.secondParentReteNode.setStateToFalse(secondParentInstanceID, this.nodeID);
		}else if(state == 2) {
			this.firstParentReteNode.setStateToTemp(firstParentInstanceID, this.nodeID);
			this.secondParentReteNode.setStateToTemp(secondParentInstanceID, this.nodeID);
		}
		
	}
	
	public synchronized void checkStateTable() {
				
		ArrayList<Long> matchedKeySet = new ArrayList<Long>();
		boolean existTrue;
		boolean existUnfinished;
		Date now = new Date();
		
		Iterator<Long> tableIterator = this.stateTable.keySet().iterator();
		while(tableIterator.hasNext()) {
			
			long firstKey = tableIterator.next();
			
			if(this.myInstanceMap.get(firstKey).getNumberOfWaitFromChild() == 0 
					&& now.getTime() - this.myInstanceMap.get(firstKey).getCreatedTime().getTime() > this.errorRange + this.maxWaitingTime) {
				
				Iterator<Long> instanceIterator = this.stateTable.get(firstKey).keySet().iterator();
				existTrue = false;
				existUnfinished = false;
				
				while(instanceIterator.hasNext()) { // state table instance 순회
					long secondKey = instanceIterator.next();
					int state = this.stateTable.get(firstKey).get(secondKey);
					
					if(state == -1 || state == 2) {
						// exist initial state or temp state
						existUnfinished = true;
						break;
					}else if(state == 1) {
						existTrue = true;
					}
				}
				
				if(existUnfinished) { // 아직 판단해야 하는 경우가 남은 경우(-1이 존재하면) 다음번에 다시 판단해야함.
				
				}else { // 판단이 끝난 경우(state table이 모두 0과 1로 채워진 경우)
					if(this.isAlphaNode()) { // alpha node인 경우
					//	System.out.println("node id: " + this.nodeID + " 판단이 끝난경우: " + this.stateTable.get(firstKey).toString() + ", first key: " + firstKey);
					//	System.out.println("child: " + this.myInstanceMap.get(firstKey).getNumberOfWaitFromChild());
						
						NodeInstance recvInstance = this.myInstanceMap.get(firstKey);
						MatchingEngine.confirmMap.remove(recvInstance.getLidStack().get(0));
					//	System.err.println(MatchingEngine.confirmMap);
						if(existTrue) {
						//	System.out.println("[Rete] RETURN : NodeID: " + recvInstance.getNodeID() + ", InstanceID: " + recvInstance.getInstanceID() + ", LidStack: " + recvInstance.getLidStack().toString() + " is normal(O).");
						}else { // state table 내용이 모두 다 0인 경우
							
							// 비정상 소요시간 측정을 위한 코드
							Date afterTimeStamp = new Date();
							Date beforeTimeStamp = (Date)MonitorMemory.timeStampMap.get(recvInstance.getLidStack().get(0)).clone();
							long requiredTime = (long)afterTimeStamp.getTime() - (long)beforeTimeStamp.getTime();
							System.out.println("비정상: lid: " + recvInstance.getLidStack().get(0) + ", requiredTime: " + requiredTime);
							MonitorMemory.timeStampMap.remove(recvInstance.getLidStack().get(0));
							synchronized(MonitorMemory.abnormalTimeCalculationLock) {
								MonitorMemory.totalOfAbnormalRequiredTime += requiredTime;
								MonitorMemory.numOfAbnormalComplete++;
							}
							
						//	System.out.println("[Rete] RETURN : NodeID: " + recvInstance.getNodeID() + ", InstanceID: " + recvInstance.getInstanceID() + ", LidStack: " + recvInstance.getLidStack().toString() + " is abnormal(X).");
							
							if(!FlowSim.abnormalMap.containsKey(recvInstance.getLidStack().get(0))) {
								System.err.println("정상을 비정상이라고 판단. lid: " + recvInstance.getLidStack().get(0));
								synchronized(MonitorMemory.wrongLock) {
									MonitorMemory.numOfWrongAnswer++;
								}
							}
							FlowSim.abnormalMap.remove(recvInstance.getLidStack().get(0)); // abnormalMap에서 제거해줌.
						}
						matchedKeySet.add(firstKey); // 삭제할 state table, myInstanceMap의 키 임시 저장.
					}else { // agg node인 경우
						NodeInstance recvInstance = this.myInstanceMap.get(firstKey);
						long firstParentInstanceID = recvInstance.getFirstParentID();
						long secondParentInstanceID = recvInstance.getSecondParentID();
						if(existTrue) { // state table에 1이 존재할 경우
							this.setStateToParents(firstParentInstanceID, secondParentInstanceID, 1); // 부모들의 state table을 1로 세팅
						}else { // state table 내용이 모두 다 0인 경우
							this.setStateToParents(firstParentInstanceID, secondParentInstanceID, 0); // 부모들의 state table을 0으로 세팅.
						}
						matchedKeySet.add(firstKey); // 삭제할 state table, myInstanceMap의 키 임시 저장.
					}
				}
			}
		}
	//	}
		
		// iterator가 끝난 후 한꺼번에 삭제해줌.
		for(int i=0; i<matchedKeySet.size(); i++) {

			synchronized(this.stateTableLock) {
				this.stateTable.remove(matchedKeySet.get(i));
			}
		//	this.stateTableTimeMap.remove(matchedKeySet.get(i));
			synchronized(this.instanceMapLock) {
				this.myInstanceMap.remove(matchedKeySet.get(i));
			}
		}
	}
	
	public int getState(long recvInstanceID, long recvNodeID) {
		synchronized(stateTableLock) {
			if(this.stateTable.containsKey(recvInstanceID)) {
				if(this.stateTable.get(recvInstanceID).containsKey(recvNodeID))
					return this.stateTable.get(recvInstanceID).get(recvNodeID);
			}
			return -2;
		}
	}
	
	public void setChildReteNode(long NodeID, ReteNode aggNode) {
		this.childReteNodeMap.put(NodeID, aggNode);
		this.childBoolMap.put(aggNode.getNodeID(), -1);
	}
	
	public long getNodeID() {
		return this.nodeID;
	}

	public boolean isAlreadyExistChild(long key) {
		if(this.childReteNodeMap.containsKey(key)) 
			return true;
		else 
			return false;
	}
	
	public ReteNode returnChild(long key) {
		return this.childReteNodeMap.get(key);
	}
	
	public boolean isAlphaNode() {
		if(this.firstParentReteNode == null && this.secondParentReteNode == null)
			return true;
		else
			return false;
	}
	
	public void setAppID(long appID) {
		this.appID = appID;
	}
	
	public void removeWaitedInstanceList() {
				
		Date now = new Date();
		ArrayList<NodeInstance> matchedInstanceList = new ArrayList<NodeInstance>();
		ArrayList<NodeInstance> alreadyMatchedInstanceList = new ArrayList<NodeInstance>();
		
		Iterator<Long> waitedListItr = this.waitedInstanceList.keySet().iterator();
		while(waitedListItr.hasNext()) {
			NodeInstance waitedInstance = this.waitedInstanceList.get(waitedListItr.next());
			
			Date instanceCreatedTime = waitedInstance.getCreatedTime();
							
			long observedDuration = (long)(now.getTime() - instanceCreatedTime.getTime());
		//	System.out.println("observed: " + observedDuration + ", sum: " + (this.profiledDuration+this.errorRange));
			if((this.profiledDuration + this.errorRange) + 1*1000 < observedDuration) { // (17.03.28) 1초의 추가 기한을 더 줌.
				int firstParentState = this.firstParentReteNode.getState(waitedInstance.getInstanceID(), this.nodeID);
				if(firstParentState == -1) { // 한번도 일치하지 않았으면 부모에 0을 반환하고 삭제해준다
					matchedInstanceList.add(waitedInstance); // 삭제할 목록 임시 배열에 저장
				//	System.out.println("ㅁㄴㅇ node ID:" + this.nodeID + ", instance ID: " + waitedInstance.getInstanceID() + " ,lid: " + waitedInstance.getLidStack());
				}else { // 기다리는 중이거나 이미 끝난경우 삭제만해준다
				//	System.out.println(firstParentState + ", node ID:" + this.nodeID + ", instance ID: " + waitedInstance.getInstanceID() + " ,lid: " + waitedInstance.getLidStack());
					alreadyMatchedInstanceList.add(waitedInstance);
				}
			}
		}
	
		// 임시저장한 인스턴스들 모두 삭제.
		for(int i=0; i<matchedInstanceList.size(); i++) {
			NodeInstance waitedInstance = matchedInstanceList.get(i);
			this.firstParentReteNode.setStateToFalse(waitedInstance.getInstanceID(), this.nodeID);
		//	synchronized(waitListLock) {
				this.waitedInstanceList.remove(waitedInstance.getInstanceID());
		//	}
		}
		for(int i=0; i<alreadyMatchedInstanceList.size(); i++) {
			NodeInstance waitedInstance = alreadyMatchedInstanceList.get(i);
		//	synchronized(waitListLock) {
				this.waitedInstanceList.remove(waitedInstance.getInstanceID());
		//	}
		}
	}
	
	public boolean isEmptyStateTable() {
		if(this.stateTable.isEmpty()) return true;
		else return false;
	}
	
	public boolean isEmptyWaitedInstanceList() {
		if(this.waitedInstanceList.isEmpty()) return true;
		else return false;
	}
	
	public boolean isContainStateTable(NodeInstance recvInstance) {
		return this.stateTable.containsKey(recvInstance.getInstanceID());
	}
	
	public long getFirstParent() {
		if(this.firstParentReteNode != null)
			return this.firstParentReteNode.getNodeID();
		else
			return -1;
	}
	
	public long getSecondParent() {
		if(this.secondParentReteNode != null)
			return this.secondParentReteNode.getNodeID();
		else
			return -1;
	}
	
	public int getNumberOfChild() {
		return this.childReteNodeMap.size();
	}
	
	public void setMaxWaitingTime(long time) {
		if(this.maxWaitingTime <= time) {
			this.maxWaitingTime = time;
		}
	}
}
