package org.apl.netbig.rete.onethread;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

public class NodeInstance {
	
	private long instanceID;
	private long nodeID;
	private long firstParentInstanceID;
	private long secondParentInstanceID;
	private ConcurrentHashMap<Long, ArrayList<Date>> flwStack = new ConcurrentHashMap<Long, ArrayList<Date>>();
	private ArrayList<Long> lidStack = new ArrayList<Long>();
	private Date createdTime;
	private int numberOfWaitFromChild;
	
	private ConcurrentHashMap<Long, Integer> waitOffsetMap;
	
	private Object lockOfChild = new Object();
			
	public NodeInstance(long instanceID, long nodeID, ConcurrentHashMap<Long, ArrayList<Date>> flwStack, long firstParentInstanceID, long secondParentInstanceID, 
			ArrayList<Long> lidStack, int numberOfChild, ConcurrentHashMap<Long, Integer> childBoolMap) {
		
		this.instanceID = instanceID;
		this.nodeID = nodeID;
		this.flwStack = flwStack;
		this.firstParentInstanceID = firstParentInstanceID;
		this.secondParentInstanceID = secondParentInstanceID;
		this.lidStack = lidStack;
		this.createdTime = new Date();
		this.numberOfWaitFromChild = numberOfChild;
		this.waitOffsetMap = new ConcurrentHashMap<Long, Integer>(childBoolMap);
	}
	
	public Date getCreatedTime() {
		return this.createdTime;
	}
	
	public ConcurrentHashMap<Long, ArrayList<Date>> getFlwStack() {
		return this.flwStack;
	}
	
	public long getInstanceID() {
		return this.instanceID;
	}
	
	public long getNodeID() {
		return this.nodeID;
	}
	
	public long getFirstParentID() {
		return this.firstParentInstanceID;
	}
	
	public long getSecondParentID() {
		return this.secondParentInstanceID;
	}
	
	public ArrayList<Long> getLidStack() {
		return this.lidStack;
	}
	
	public int getNumberOfWaitFromChild() {
		return this.numberOfWaitFromChild;
	}
	
	
	public void addNumberOfWaitFromChild(long nodeID) {
		synchronized(lockOfChild) {
			if(this.waitOffsetMap.get(nodeID) == -1) {
				this.waitOffsetMap.put(nodeID, 1);
			}
			else {
				this.numberOfWaitFromChild++;
			}
		}
	}
	
	public void reduceNumberOfWaitFromChild() {
		synchronized(lockOfChild) {
			this.numberOfWaitFromChild--;
		}
	}
}
