package org.apl.netbig.rete.onethread;

import java.util.Iterator;
import java.util.TimerTask;

public class TaskOfCheckStateTable extends TimerTask{
	
	@Override
	public synchronized void run() {
		
		Iterator<Long> itr = MatchingEngine.stateTableList.iterator();
		
		while(itr.hasNext()) {
			
			long key = itr.next();
			ReteNode rete = MatchingEngine.matchingHashMap.get(key);
			
			rete.checkStateTable();
		}
		
	}
}
