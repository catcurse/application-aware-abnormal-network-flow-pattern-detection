package org.apl.netbig.rete.busywaiting;

import java.util.Timer;
import java.util.TimerTask;

public class TaskOfRemoveWaitedInstanceBusy extends TimerTask{

	private ReteNodeBusy recvReteNode;
	private Timer recvTimer;
	
	public TaskOfRemoveWaitedInstanceBusy(ReteNodeBusy recvReteNode, Timer recvTimer) {
		this.recvReteNode = recvReteNode;
		this.recvTimer = recvTimer;
	}
	
	// (17.03.28) 타이머가 필요할 때만 동작하게 수정.
	@Override
	public synchronized void run() {
	//	if(this.recvReteNode.isEmptyWaitedInstanceList()) { // waited instance list가 비어있으면 timer task 종료.
	//		this.recvTimer.cancel();
	//		this.recvTimer.purge();
	//		synchronized(this.recvReteNode.removeLock) {
	//			this.recvReteNode.removeBool = false;
	//		}
	//		this.recvReteNode.setIsRunRemoveTask(false);
	//	}else { // waited instance list가 채워져 있으면,
	//		this.recvReteNode.setIsRunRemoveTask(true);
	//		synchronized(this.recvReteNode.removeLock) {
	//			this.recvReteNode.removeBool = true;
	//		}
			this.recvReteNode.removeWaitedInstanceList();
	//		System.out.println("waited list: " + this.recvReteNode.waitedInstanceList.toString() + ", node id: " + this.recvReteNode.getNodeID());
	//	}
	}
	
}
