package org.apl.netbig.rete.nodethread;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apl.netbig.execute.MonitorMemory;
import org.apl.netbig.platformx.FlowSim;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MatchingEngine implements Runnable {
	
	static SimpleDateFormat formatterMs = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");

	private String whiteListInputFileName;	
	protected static ConcurrentHashMap<Long, ReteNode> matchingHashMap = new ConcurrentHashMap<Long, ReteNode>();
	private static ConcurrentHashMap<Long, Long> alphaNodeMap = new ConcurrentHashMap<Long, Long>();
	
	
	public static ConcurrentHashMap<Long, Long> confirmMap = new ConcurrentHashMap<Long, Long>(); // 생성된 플로우들이 모두 잘 지워지는 지 판단하기 위해 lid값을 임시로 저장해놓은 자료구조.
	
	private static long nodeID = 0;
	private static long seqID = 0;
	
	private static long alphaNum = 0;
	private static long aggNum = 0;
	private static long leafNum = 0;
	
	
	private synchronized static long genNodeID() {
		nodeID++;
		return nodeID;
	}
	
	
	public static long instanceID = 0;
	public synchronized static long genInstanceID() {
		instanceID++;
		return instanceID;
	}
	
	private synchronized static long genSeqID() {
		seqID++;
		return seqID;
	}
	
	public MatchingEngine(String whiteListInputFileName) {
		this.whiteListInputFileName = whiteListInputFileName;
	}
	
	public static void constructReteNetwork(String whiteListInputFileName) {
		JSONParser jsonParser = new JSONParser();
		FileReader patternReader = null;
		BufferedReader bufferedpatternReader = null;
		
	//	long duplicatedWhiteListNum = 0;
		
		try {
			patternReader = new FileReader(whiteListInputFileName);
			bufferedpatternReader = new BufferedReader(patternReader);

			String whiteListLine = null;
			
			long checkTime = 1*1000;
			long removeTime = 1*1000;
			long errorRange = 500; // (17.04.06) errorRange 수정.
			
			
            // make whiteList
			while(true) { // 전체 파일 순회
				whiteListLine = bufferedpatternReader.readLine();
				if(whiteListLine == null) break;
				
				long previousKey = -1;
				
				JSONObject whiteObject = (JSONObject)jsonParser.parse(whiteListLine); // 파일 한 줄 parsing
				
				// 각 요소를 가져옴(parsing)
				long appID = Long.parseLong(whiteObject.get("app").toString());
				JSONArray flwList = (JSONArray)whiteObject.get("flw"); // flw 리스트 가져옴
				JSONArray durationList = (JSONArray)whiteObject.get("duration"); // duration 리스트 가져옴
		
				for (int i = 0; i < flwList.size(); i++) { // flow 리스트 순회
					//System.out.println("p: " + previousKey);
					//System.out.println("r: " + jsonObject);
					//System.out.println("m: " + matchingHashMap.toString());
					//System.out.println("a: " + alphaNodeMap.toString());
					
					long duration = -1; // 맨 처음 노드인 경우 duration은 -1
					long flwNum = (long)flwList.get(i);

					if(alphaNodeMap.containsKey(flwNum)) { // 이미 만들려던 알파노드가 존재한다면,
						ReteNode alphaNode = matchingHashMap.get(alphaNodeMap.get(flwNum));
						
						if(i == 0) { // 맨 처음 노드인 경우, AggNode 생성 x
							previousKey = alphaNodeMap.get(flwNum);
						}else { // 맨 처음 노드가 아닌 경우 AggNode가 원래 존재할 경우와 존재하지 않는 경우로 나눔.
							duration = (long)durationList.get(i-1) * 1000;
						}
						
						if(i != flwList.size()-1) { // Agg Node인 경우.
							if(i != 0) { // 첫번째 노드인 경우에는 AggNode 생성하지 않음.
								if(alphaNode.isAlreadyExistChild(previousKey)) { // AggNode가 이미 존재할 경우.
									previousKey = alphaNode.returnChild(previousKey).getNodeID();
								}else { // AggNode가 원래 존재하지 않을 경우.
									
									ReteNode aggNode = new ReteNode(genNodeID(), matchingHashMap.get(previousKey), alphaNode, false, duration, errorRange);
									aggNum++;
									matchingHashMap.put(aggNode.getNodeID(), aggNode);
									
									// set nextNode
									alphaNode.setChildReteNode(matchingHashMap.get(previousKey).getNodeID(), aggNode);
									matchingHashMap.get(previousKey).setChildReteNode(aggNode.getNodeID(), aggNode);
									previousKey = aggNode.getNodeID(); // 맨 처음 노드가 아닐 경우 previous key에 최신 aggNode의 nodeID 저장.
									
								//	Timer removeTimer = new Timer();
								//	removeTimer.schedule(new TaskOfRemoveWaitedInstance(aggNode, removeTimer), 0, removeTime/*(this.profiledDuration + this.errorRange)*/);
								//	Timer checkTimer = new Timer();
								//	checkTimer.schedule(new TaskOfCheckStateTable(aggNode, checkTimer), 0, checkTime);
								}
							}
						}else { // 리프노드인 경우.
						//	if(!alphaNode.isAlreadyExistChild(previousKey)) { // 이미 이 리프노드가 존재하지 않는다면 (중복되는 whiteList 없음)
							
							/*
							 * 중복체크는 나중에 하기로 함.
							 * 현재 버전에서는 인풋에서 중복되는 Leaf 노드는 등장하지 않는것으로 가정.
							 * Leaf 노드를 자식으로 가리킬 때 두번째 부모 노드(Alpha 노드) 역시 자식을 가리키는 HashMap에 Key로 해당 Leaf 노드의 ID를 줌.
							 */
							
							ReteNode leafNode = new ReteNode(genNodeID(), matchingHashMap.get(previousKey), alphaNode, true, duration, errorRange);
							leafNum++;
							leafNode.setAppID(appID);
							matchingHashMap.put(leafNode.getNodeID(), leafNode);
							
						//	alphaNode.setChildReteNode(matchingHashMap.get(previousKey).getNodeID(), leafNode);
							alphaNode.setChildReteNode(leafNode.getNodeID(), leafNode);
							matchingHashMap.get(previousKey).setChildReteNode(leafNode.getNodeID(), leafNode);
							
						//	Timer removeTimer = new Timer();
						//	removeTimer.schedule(new TaskOfRemoveWaitedInstance(leafNode, removeTimer), 0, removeTime/*(this.profiledDuration + this.errorRange)*/);
							
						//	} else { // 이미 이 리프노드가 존재한다면  몇개나 존재하는지 체크(중복되는 whiteList 있음)
						//		duplicatedWhiteListNum++; // 중복되는 화이트리스트 나타내는 변수 1 추가
						//	}
						}
					} else { // 존재하지 않으면 alpha 노드를 새로 생성.
						long alphaNodeID = genNodeID();
						ReteNode alphaNode = new ReteNode(alphaNodeID, null, null, false, (long)-1, (long)-1);
						alphaNum++;
						matchingHashMap.put(alphaNodeID, alphaNode);
						alphaNodeMap.put(flwNum, alphaNodeID);
						
					//	Timer checkTimer = new Timer();
					//	checkTimer.schedule(new TaskOfCheckStateTable(alphaNode, checkTimer), 0, checkTime);
						
						if(i == 0) { // 맨 처음 노드인 경우.
							previousKey = alphaNode.getNodeID(); // 맨 처음 노드인 경우 previous key에 alphaNode의 nodeID 저장.
						}else { // 맨 처음 노드가 아닌 경우, 듀레이션 계산.
							duration = (long)durationList.get(i-1) * 1000;
						}
						
						if(i != flwList.size()-1) { // 리프노드가 아닌경우.
							if(i != 0) { // 첫번째 노드인 경우에는 AggNode 생성하지 않음.
								ReteNode aggNode = new ReteNode(genNodeID(), matchingHashMap.get(previousKey), alphaNode, false, duration, errorRange);
								aggNum++;
								matchingHashMap.put(aggNode.getNodeID(), aggNode);
								
								// set childNode
								alphaNode.setChildReteNode(matchingHashMap.get(previousKey).getNodeID(), aggNode);
								matchingHashMap.get(previousKey).setChildReteNode(aggNode.getNodeID(), aggNode);
								previousKey = aggNode.getNodeID(); // 맨 처음 노드가 아닐 경우 previous key에 최신 aggNode의 nodeID 저장.
								
							//	Timer removeTimer = new Timer();
							//	removeTimer.schedule(new TaskOfRemoveWaitedInstance(aggNode, removeTimer), 0, removeTime/*(this.profiledDuration + this.errorRange)*/);
								
							//	Timer checkTimer2 = new Timer();
							//	checkTimer2.schedule(new TaskOfCheckStateTable(aggNode, checkTimer2), 0, checkTime);
							}
						}else { // 리프노드인 경우.
							ReteNode leafNode = new ReteNode(genNodeID(), matchingHashMap.get(previousKey), alphaNode, true, duration, errorRange);
							leafNum++;
							leafNode.setAppID(appID);
							matchingHashMap.put(leafNode.getNodeID(), leafNode);
							
						//	alphaNode.setChildReteNode(matchingHashMap.get(previousKey).getNodeID(), leafNode);
							alphaNode.setChildReteNode(leafNode.getNodeID(), leafNode);
							matchingHashMap.get(previousKey).setChildReteNode(leafNode.getNodeID(), leafNode);
							
						//	Timer removeTimer = new Timer();
						//	removeTimer.schedule(new TaskOfRemoveWaitedInstance(leafNode, removeTimer), 0, removeTime/*(this.profiledDuration + this.errorRange)*/);
							
						}
					}
					//prevEndDate = formatter.parse(jsonObject.get("e_date").toString()); // 다음 듀레이션 계산을 위한 previous e_date 저장.
				}

			//	System.out.println("All Node Map: " + matchingHashMap.toString());
			//	System.out.println("Alpha Node Map: " + alphaNodeMap.toString());
				
			} // end of while
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("[matching Engine]: Rete Network is constructed all");
		System.out.println("[matching Engine]: alpha node: " + alphaNum);
		System.out.println("[matching Engine]: agg node: " + aggNum);
		System.out.println("[matching Engine]: leaf node: " + leafNum);
	
		//MonitorMemory.showMemory(); // 메모리 테스트 위해 네트워크 크기 출
		
	//	System.out.println("[matching Engine]: Amount of Duplicated WhiteList is " + duplicatedWhiteListNum);
		/*
		Iterator<Long> keys = matchingHashMap.keySet().iterator();
		while(keys.hasNext()) {
			long key = keys.next();
			ReteNode testNode = matchingHashMap.get(key);
			System.out.println(testNode.getNodeID() + ", " + testNode.getFirstParent() + ", " + testNode.getSecondParent() + ", ");
			
			Iterator<Long> keys2 = testNode.childReteNodeMap.keySet().iterator();
			while(keys2.hasNext()) {
				long key2 = keys2.next();
				System.out.println(testNode.childReteNodeMap.get(key2).getNodeID());
				System.out.println(testNode.childBoolMap.toString());
			}

		}
		*/
	}
	
	public static void matchingEngine(String flowInputFileName) {
		JSONParser jsonParser = new JSONParser();
		FileReader patternReader = null;
		BufferedReader bufferedpatternReader = null;
		
		// file input & json parsing to read flow--------------------------------------------------------------
		FileReader flowReader = null;
		BufferedReader bufferedFlowReader = null;
		
		try {
			flowReader = new FileReader(flowInputFileName);
			bufferedFlowReader = new BufferedReader(flowReader);
			String flow = new String();			
			
			while(true) {
				flow = bufferedFlowReader.readLine();
				
				if(flow == null) break;
				
				JSONObject jsonObject = (JSONObject) jsonParser.parse(flow);
				// abnormal pattern remove when read file
			//	if(Integer.parseInt(jsonObject.get("app").toString()) == 1 || Integer.parseInt(jsonObject.get("app").toString()) == 3) {
				
				//	System.out.println("[MatchingEngine] INPUT : " + jsonObject);
					
					long flwNum = (long)Long.parseLong(jsonObject.get("flw").toString());
					
					Date s_date = formatterMs.parse(jsonObject.get("s_date").toString());
					Date e_date = formatterMs.parse(jsonObject.get("e_date").toString());
					long lid = Long.parseLong(jsonObject.get("lid").toString());
					
					if(!alphaNodeMap.containsKey(flwNum)) {
						System.out.println("[MatchingEngine] RETURN : " + lid + " is abnormal(X).");
						FlowSim.abnormalMap.remove(lid); // abnormalMap에서 제거해줌.
					}else {
					//	System.out.println(matchingHashMap.get(alphaNodeMap.get(flwNum)).getNodeID());
						matchingHashMap.get(alphaNodeMap.get(flwNum)).trigger(s_date, e_date, lid);
					//	Thread.sleep(100);
					}
			//	}
				
			} // end of while
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void realTimeMatchingEngine() {
		
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "test");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Arrays.asList("test"));
		
		JSONParser parser = new JSONParser();

		while (true) {
			try {
				ConsumerRecords<String, String> records = consumer.poll(100);
				for (ConsumerRecord<String, String> record : records) {
				//	System.out.println(record.value());
				//	JSONParser parser = new JSONParser();
					JSONObject json = (JSONObject)parser.parse(record.value());
										
					JSONObject jsonObject = new JSONObject();
		
					jsonObject.put("lid", json.get("lid"));
					jsonObject.put("flw", json.get("flw"));
					jsonObject.put("app", json.get("app"));
					jsonObject.put("s_date", json.get("s_date"));
					jsonObject.put("e_date", json.get("e_date"));
				//	System.out.println("[MatchingEngine] INPUT : " + jsonObject);

					
					long flwNum = (long)Long.parseLong(jsonObject.get("flw").toString());
					
					Date s_date = null;
					Date e_date = null;
					try {
						s_date = formatterMs.parse(jsonObject.get("s_date").toString());
						e_date = formatterMs.parse(jsonObject.get("e_date").toString());
					} catch (java.text.ParseException e) {
						e.printStackTrace();
					}
					long lid = Long.parseLong(jsonObject.get("lid").toString());
					
					
					if(!alphaNodeMap.containsKey(flwNum)) {
						System.out.println("[MatchingEngine] RETURN : [" + lid + "] is abnormal(X).");
						if(!FlowSim.abnormalMap.containsKey(lid)) System.err.println("망함. " + lid);
						FlowSim.abnormalMap.remove(lid); // abnormalMap에서 제거해줌.
					}else {
					//	System.out.println(matchingHashMap.get(alphaNodeMap.get(flwNum)).getNodeID());
						
					 	confirmMap.put(lid, (long)0);
						
						long alphaNodeID = alphaNodeMap.get(flwNum);
					//	Timer triggerTimer = new Timer();
					//	triggerTimer.schedule(new TaskOfTrigger(alphaNodeID, s_date, e_date, lid), 1000);
						
						matchingHashMap.get(alphaNodeID).trigger(s_date, e_date, lid); // 트리거를 날려줌.
						MonitorMemory.timeStampMap.put(lid, new Date());	// 타임스탬프 정보를 저장.
					}
					jsonObject.clear();
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void run() {
				
		Date before = new Date();
		System.out.println("[matching Engine]: constructReteNetwork start: " + formatterMs.format(before));
		this.constructReteNetwork(whiteListInputFileName);
		Date after = new Date();
		System.out.println("[matching Engine]: constructReteNetwork end: " + formatterMs.format(after));
		System.out.println("[matching Engine]: constructReteNetwork time: " + (long)(after.getTime() - before.getTime()) + "ms");
		this.realTimeMatchingEngine();
	}
	
}
