package org.apl.netbig.rete.nodethread;

import java.util.Timer;
import java.util.TimerTask;

public class TaskOfCheckStateTable extends TimerTask{
	
	private ReteNode recvReteNode;
	private Timer recvTimer;

	public TaskOfCheckStateTable(ReteNode recvReteNode, Timer recvTimer) {
		this.recvReteNode = recvReteNode;
		this.recvTimer = recvTimer;
	}

	// (17.03.28) 타이머가 필요할 때만 동작하게 수정.
	@Override
	public synchronized void run() {
		if(this.recvReteNode.isEmptyStateTable()) { // state table이 비어있으면 timer task 종료.
			this.recvTimer.cancel();
			this.recvTimer.purge();
			synchronized(this.recvReteNode.checkLock) {
				this.recvReteNode.checkBool = false;
			}
	//		this.recvReteNode.setIsRunCheckTask(false);
		}else { // state table에 채워져있으면 task 실행.
	//		this.recvReteNode.setIsRunCheckTask(true);
			synchronized(this.recvReteNode.checkLock) {
				this.recvReteNode.checkBool = true;
			}
			this.recvReteNode.checkStateTable();
	//		System.out.println("state: " + this.recvReteNode.stateTable.toString() + ", node id: " + this.recvReteNode.getNodeID());
		}
	}
}
